﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentNHibernate.Mapping;

namespace ProductManagerModels
{
    public class Product
    {
        public virtual Guid ID { get; set; }
        public virtual string SKU { get; set; }
        public virtual string CustomerSKU { get; set; }
        public virtual string CustomerNumber { get; set; }
        public virtual string BasePartNumber { get; set; }
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
        public virtual string Applications { get; set; }
        public virtual string MarketingCopy { get; set; }
        public virtual string UnitOfMeasure { get; set; }
        public virtual string Availability { get; set; }
        public virtual double Price { get; set; }
        public virtual string Photo { get; set; }
    }

    public sealed class ProductMapping : ClassMap<Product>
    {
        public ProductMapping()
        {
            LazyLoad();
            Table("PortalItem");
            //Id(p => p.ID, "ID").GeneratedBy.Assigned();
            //Map(p => p.SKU, "Number");
            Id(p => p.SKU, "Number").GeneratedBy.Assigned();
            Map(p => p.CustomerSKU, "CustomerPadrtNumber");
            Map(p => p.CustomerNumber);
            Map(p => p.BasePartNumber);
            Map(p => p.Name);
            Map(p => p.Description);
            Map(p => p.Applications);
            Map(p => p.MarketingCopy);
            Map(p => p.UnitOfMeasure);
            Map(p => p.Availability);
            Map(p => p.Price);
            Map(p => p.Photo);
        }
    }
}