﻿using System;
using FluentNHibernate.Mapping;

namespace SAPModels
{
    public class Customer
    {
        public virtual string Number { get; set; }
        public virtual string ParentNumber { get; set; }
        public virtual string Name { get; set; }
        public virtual string Salesman { get; set; }
        public virtual string RepGroup { get; set; }
    }

    public sealed class CustomerMapping : ClassMap<Customer>
    {
        public CustomerMapping()
        {
            ReadOnly();
            LazyLoad();
            Table("PRD.prd.ZV_KNA1_KNVV");
            Id(p => p.Number, "KUNNR");
            Map(p => p.Name, "NAME1");
            Map(p => p.ParentNumber, "KUNNR2");
            Map(p => p.Salesman, "SMTP_ADDR");
            Map(p => p.RepGroup, "VKGRP");
        }
    }
}
