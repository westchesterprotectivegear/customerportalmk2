﻿using System;
using FluentNHibernate.Mapping;

namespace SAPModels
{
    public class ShipTo
    {
        public virtual string ID { get; set; }
        public virtual string State { get; set; }
        public virtual string City { get; set; }
        public virtual string Street { get; set; }
        public virtual string Zip { get; set; }
        public virtual string Name { get; set; }
    }

    public sealed class ShipToMapping : ClassMap<ShipTo>
    {
        public ShipToMapping()
        {
            ReadOnly();
            LazyLoad();
            Where("MANDT = '100'");
            Table("PRD.prd.KNA1");
            Id(p => p.ID, "KUNNR");
            Map(p => p.State, "REGIO");
            Map(p => p.City, "ORT01");
            Map(m => m.Street, "STRAS");
            Map(m => m.Zip, "PSTLZ");
            Map(p => p.Name, "NAME1");
        }
    }
}
