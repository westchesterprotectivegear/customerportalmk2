﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentNHibernate.Mapping;

namespace SAPModels
{
    public class Order
    {
        public virtual string SalesOrder { get; set; }
        public virtual string PurchaseOrder { get; set; }
        public virtual string SoldTo { get; set; }
        public virtual string OrderDateSAP { get; set; }
        public virtual string TimeEntered { get; set; }
        public virtual double OrderValue { get; set; }
        public virtual string PromisedDate { get; set; }
        public virtual string Name { get; set; }
        public virtual string ShipTo { get; set; }
        public virtual string StatusCode { get; set; }
        public virtual List<OrderLine> LineItems { get; set; }
        public virtual string ShipName { get; set; }
        public virtual string ShipStreet { get; set; }
        public virtual string ShipCity { get; set; }
        public virtual string ShipState { get; set; }
        public virtual string ShipZip { get; set; }
        public virtual string ShipName2 { get; set; }
        public virtual List<PackingSlip> PackingSlips { get; set; }

        public virtual string Status
        {
            get
            {
                string status = "";
                switch (StatusCode)
                {
                    case "A":
                        status = "Not yet shipped";
                        break;
                    case "B":
                        status = "Partially shipped";
                        break;
                    case "C":
                        status = "Completely shipped";
                        break;
                    default:
                        status = "Unknown";
                        break;
                }
                return status;
            }
        }

        public virtual DateTime OrderDate
        {
            get
            {
                return DateTime.ParseExact(OrderDateSAP, "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture);
            }
        }

        public virtual double TotalWeight
        {
            get
            {
                if (LineItems != null)
                    return (from x in LineItems select x.LineWeight).Sum();
                return 0;
            }
        }

        public virtual double TotalVolume
        {
            get
            {
                if (LineItems != null)
                    return (from x in LineItems select x.LineVolume).Sum();
                return 0;
            }
        }
    }

    public sealed class OrderMapping : ClassMap<Order>
    {
        public OrderMapping()
        {
            ReadOnly();
            LazyLoad();
            Table("PRD.prd.ZV_VAKPA_VBAK");
            Id(p => p.SalesOrder, "VBELN");
            Map(p => p.PurchaseOrder, "BSTNK");
            Map(p => p.SoldTo, "KUNDE");
            Map(m => m.OrderDateSAP, "ERDAT");
            Map(m => m.TimeEntered, "ERZET");
            Map(p => p.OrderValue, "NETWR");
            Map(p => p.PromisedDate, "VDATU");
            Map(m => m.Name, "NAME1");
            Map(m => m.ShipTo, "KUNNR");
            Map(m => m.StatusCode, "LFSTK");
            Map(f => f.ShipName, "Expr1");
            Map(f => f.ShipStreet, "STREET");
            Map(f => f.ShipCity, "CITY1");
            Map(f => f.ShipState, "REGION");
            Map(f => f.ShipZip, "POST_CODE1");
            Map(f => f.ShipName2, "NAME2");
        }
    }
}