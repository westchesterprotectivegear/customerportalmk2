﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentNHibernate.Mapping;

namespace SAPModels
{
    public class OrderLine
    {
        public virtual string OrderNumber { get; set; }
        public virtual string LineNumber { get; set; }
        public virtual string SoldName { get; set; }
        public virtual string SoldStreet { get; set; }
        public virtual string SoldCity { get; set; }
        public virtual string SoldState { get; set; }
        public virtual string SoldZip { get; set; }
        public virtual string SoldName2 { get; set; }
        public virtual string OrderEntered { get; set; }
        public virtual string PONumber { get; set; }
        public virtual string PODate { get; set; }
        public virtual string FreightTerms { get; set; }
        public virtual string PaymentTerms { get; set; }
        public virtual string SKU { get; set; }
        public virtual string CustomerSKU { get; set; }
        public virtual double LineWeight { get; set; }
        public virtual double LineVolume { get; set; }
        public virtual string PromisedDate { get; set; }
        public virtual string Description { get; set; }
        public virtual int OrderQuantity { get; set; }
        public virtual double SellPrice { get; set; }
        public virtual string UnitOfMeasure { get; set; }
        public virtual int Confirmed { get; set; }
        public virtual string SalesPerson { get; set; }
        public virtual string Currency { get; set; }
        public virtual string CustomerPONumber { get; set; }
        public virtual string OrderType { get; set; }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    public sealed class OrderLineMapping : ClassMap<OrderLine>
    {
        public OrderLineMapping()
        {
            ReadOnly();
            LazyLoad();

            Table("prd.ZV_VBAK_VBAP");

            CompositeId()
                .KeyProperty(m => m.OrderNumber, "VBELN")
                .KeyProperty(m => m.LineNumber, "POSNR");
            Id(m => m.OrderNumber, "VBELN");
            Map(m => m.LineNumber, "POSNR");
            Map(m => m.SoldName, "NAME1");
            Map(m => m.SoldStreet, "STREET");
            Map(m => m.SoldCity, "CITY1");
            Map(m => m.SoldState, "REGION");
            Map(m => m.SoldZip, "POST_CODE1");
            Map(m => m.SoldName2, "NAME2");
            Map(m => m.OrderEntered, "ERDAT");
            Map(m => m.PONumber, "BSTNK");
            Map(m => m.PODate, "BSTDK");
            Map(m => m.FreightTerms, "INCO1");
            Map(m => m.PaymentTerms, "TEXT1");
            Map(m => m.SKU, "MATNR");
            Map(m => m.CustomerSKU, "KDMAT");
            Map(m => m.LineWeight, "NTGEW");
            Map(m => m.LineVolume, "VOLUM");
            Map(m => m.PromisedDate, "VDATU");
            Map(m => m.Description, "ARKTX");
            Map(m => m.OrderQuantity, "KWMENG");
            Map(m => m.SellPrice, "NETPR");
            Map(m => m.UnitOfMeasure, "MSEH3");
            Map(m => m.Confirmed, "KBMENG");
            Map(m => m.SalesPerson, "BEZEI");
            Map(m => m.Currency, "WAERK");
            Map(m => m.CustomerPONumber, "BSTKD_E");
            Map(m => m.OrderType, "VBTYP");
        }
    }
}