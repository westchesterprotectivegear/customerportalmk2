﻿using System;
using FluentNHibernate.Mapping;

namespace SAPModels
{
    public class PackingSlip
    {
        public virtual string ID { get; set; }
        public virtual string DeliveryNumber { get; set; }
        public virtual string TrackingNumber { get; set; }
        public virtual string TrackingLink { get; set; }
        public virtual string DeliveryDate { get; set; }
        public virtual string Carrier { get; set; }

        public virtual string DeliveryDateFormatted
        {
            get
            {
                return DateTime.ParseExact(DeliveryDate, "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture).ToShortDateString();
            }
        }
    }
}
