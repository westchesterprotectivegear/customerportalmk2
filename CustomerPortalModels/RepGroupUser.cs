﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace CustomerPortalModels
{
    public class RepGroupUser : User
    {
        public virtual string DefaultShipTo { get; set; }
        public virtual string RepGroupNumber { get; set; }
        public virtual string AccountManager { get; set; }
        public virtual string SalesRep { get; set; }

        public override string AccountType
        {
            get
            {
                return "Rep Group";
            }
        }
    }

    public sealed class RepGroupUserMapping : ClassMap<RepGroupUser>
    {
        public RepGroupUserMapping()
        {
            LazyLoad();
            Table("UserRepGroup");
            Id(p => p.UserName).GeneratedBy.Assigned();
            Map(p => p.DisplayName);
            Map(p => p.RepGroupNumber);
            Map(p => p.DefaultShipTo);
            Map(p => p.AccountManager);
            Map(p => p.SalesRep);
        }
    }
}
