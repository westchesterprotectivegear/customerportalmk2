﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using System.Globalization;

namespace CustomerPortalModels
{
    public class CartLineItem
    {
        public virtual int ID { get; set; }
        public virtual int Quantity { get; set; }
        public virtual string SKU { get; set; }
        public virtual string UserName { get; set; }
        public virtual bool Sample { get; set; }
        public virtual Product Product { get; set; }

        public virtual double TotalPrice
        {
            get
            {
                return Product.DiscountedPrice * Quantity;
            }
        }

        public virtual int StockQuantity { get; set; }
    }

    public sealed class CartLineItemMapping : ClassMap<CartLineItem>
    {
        public CartLineItemMapping()
        {
            LazyLoad();
            Table("ShoppingCartLineItem");
            Id(p => p.ID).GeneratedBy.Identity();
            Map(p => p.Quantity);
            Map(p => p.SKU);
            Map(p => p.UserName);
            Map(p => p.Sample);
        }
    }
}
