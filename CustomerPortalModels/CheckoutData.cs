﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentNHibernate.Mapping;

namespace CustomerPortalModels
{
    public class CheckoutData
    {
        public virtual Guid ID { get; set; }
        public virtual string Data { get; set; }
        public virtual string UserName { get; set; }
        public virtual string NotificationEmail { get; set; }
        public virtual string OrderNumber { get; set; }
        public virtual string Browser { get; set; }
        public virtual bool Success { get; set; }
        public virtual DateTime Date { get; set; }
    }

    public sealed class CheckoutDataMapping : ClassMap<CheckoutData>
    {
        public CheckoutDataMapping()
        {
            LazyLoad();
            Table("CheckoutData");
            Id(p => p.ID, "ID").GeneratedBy.Guid();
            Map(p => p.Data).CustomType("StringClob").CustomSqlType("varchar(max)");
            Map(p => p.UserName);
            Map(p => p.OrderNumber);
            Map(p => p.Browser);
            Map(p => p.Success);
            Map(p => p.Date).Generated.Always();
            Map(p => p.NotificationEmail);
        }
    }
}