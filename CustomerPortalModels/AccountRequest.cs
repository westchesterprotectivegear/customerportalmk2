﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace CustomerPortalModels
{
    public class AccountRequest
    {
        public virtual string Email { get; set; }
        public virtual string Name { get; set; }
        public virtual string Company { get; set; }
        public virtual string Phone { get; set; }
        public virtual string Street { get; set; }
        public virtual string City { get; set; }
        public virtual string State { get; set; }
        public virtual string Zip { get; set; }
        public virtual bool MailingList { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual string Status { get; set; }
    }

    public sealed class AccountRequestMapping : ClassMap<AccountRequest>
    {
        public AccountRequestMapping()
        {
            LazyLoad();
            Table("AccountRequest");
            Id(p => p.Email).GeneratedBy.Assigned();
            Map(p => p.Name);
            Map(p => p.Company);
            Map(p => p.Phone);
            Map(p => p.Street);
            Map(p => p.City);
            Map(p => p.State);
            Map(p => p.Zip);
            Map(p => p.MailingList);
            Map(p => p.Date);
            Map(p => p.Status);
        }
    }
}
