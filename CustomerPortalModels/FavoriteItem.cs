﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace CustomerPortalModels
{
    public class FavoriteItem
    {
        public virtual string UserName { get; set; }
        public virtual string SKU { get; set; }
        public virtual DateTime Date { get; set; }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    public sealed class FavoriteItemMapping : ClassMap<FavoriteItem>
    {
        public FavoriteItemMapping()
        {
            LazyLoad();
            Table("FavoriteItem");
            CompositeId()
                .KeyProperty(m => m.UserName)
                .KeyProperty(m => m.SKU);
            Map(p => p.Date).Generated.Always();
        }
    }
}
