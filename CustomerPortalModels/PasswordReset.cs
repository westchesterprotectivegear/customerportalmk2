﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentNHibernate.Mapping;

namespace CustomerPortalModels
{
    public class PasswordReset
    {
        public virtual int ID { get; set; }
        public virtual Guid PrivateKey { get; set; }
        public virtual string UserName { get; set; }
        public virtual DateTime Date { get; set; }
    }

    public sealed class PasswordResetMapping : ClassMap<PasswordReset>
    {
        public PasswordResetMapping()
        {
            LazyLoad();
            Table("PasswordReset");
            Id(p => p.ID, "ID");
            Map(p => p.PrivateKey);
            Map(p => p.UserName);
            Map(p => p.Date).Generated.Always();
        }
    }
}