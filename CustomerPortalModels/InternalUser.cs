﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace CustomerPortalModels
{
    public class InternalUser : User
    {
        public override string AccountType
        {
            get
            {
                return "Internal";
            }
        }
    }

    public sealed class InternalUserMapping : ClassMap<InternalUser>
    {
        public InternalUserMapping()
        {
            LazyLoad();
            Table("UserInternal");
            Id(p => p.UserName).GeneratedBy.Assigned();
            Map(p => p.DisplayName);
        }
    }
}
