﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CustomerPortalModels
{
    public class User
    {
        public virtual string UserName { get; set; }
        public virtual string DisplayName { get; set; }
        public virtual string CustomerNumber { get; set; }
        public virtual string AccountType { get; set; }
    }
}
