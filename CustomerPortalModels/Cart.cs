﻿using System.Collections.Generic;
using System.Linq;
using FluentNHibernate.Mapping;

namespace CustomerPortalModels
{
    public class Cart
    {
        public virtual int ID { get; set; }
        public virtual string UserName { get; set; }
        public virtual IList<CartLineItem> LineItems { get; set; }


        public Cart()
        {
            LineItems = new List<CartLineItem>();
        }
    }

    public sealed class CartMapping : ClassMap<Cart>
    {
        public CartMapping()
        {
            LazyLoad();
            Table("ShoppingCart");
            Id(p => p.ID).GeneratedBy.Assigned();
            Map(p => p.UserName);
            HasMany(p => p.LineItems).KeyColumn("CartID");
        }
    }
}