﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace CustomerPortalModels
{
    public class UserActivity
    {
        public virtual int ID { get; set; }
        public virtual string User { get; set; }
        public virtual string Action { get; set; }
        public virtual string Message { get; set; }
        public virtual DateTime Date { get; set; }
    }

    public sealed class UserActivityMapping : ClassMap<UserActivity>
    {
        public UserActivityMapping()
        {
            LazyLoad();
            Table("UserActivity");
            Id(p => p.ID, "ID").GeneratedBy.Identity();
            Map(p => p.User, "[User]");
            Map(p => p.Action);
            Map(p => p.Message);
            Map(p => p.Date).Generated.Always();
        }
    }
}
