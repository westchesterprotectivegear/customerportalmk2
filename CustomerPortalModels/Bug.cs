﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentNHibernate.Mapping;

namespace CustomerPortalModels
{
    public class Bug
    {
        public virtual Guid ID { get; set; }
        public virtual string Title { get; set; }
        public virtual string UserName { get; set; }
        public virtual string Status { get; set; }
        public virtual IList<BugComment> Comments { get; set; }
        public virtual DateTime Date { get; set; }
    }

    public sealed class BugMapping : ClassMap<Bug>
    {
        public BugMapping()
        {
            LazyLoad();
            Table("Bug");
            Id(p => p.ID, "ID").GeneratedBy.Guid();
            Map(p => p.Title);
            Map(p => p.UserName);
            Map(p => p.Status);
            Map(p => p.Date).Generated.Always();
            HasMany(p => p.Comments).KeyColumn("BugID");
        }
    }

    public class BugComment
    {
        public virtual Guid ID { get; set; }
        public virtual Guid BugID { get; set; }
        public virtual string Text { get; set; }
        public virtual string UserName { get; set; }
        public virtual DateTime Date { get; set; }
    }

    public sealed class BugCommentMapping : ClassMap<BugComment>
    {
        public BugCommentMapping()
        {
            LazyLoad();
            Table("BugComment");
            Id(p => p.ID, "ID").GeneratedBy.Guid();
            Map(p => p.BugID);
            Map(p => p.Text);
            Map(p => p.UserName);
            Map(p => p.Date).Generated.Always();
        }
    }
}