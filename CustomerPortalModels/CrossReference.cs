﻿using System;
using FluentNHibernate.Mapping;

namespace ProductManagerModels
{
    public class CrossReference
    {
        public virtual string ID { get; set; }
        public virtual string BasePartNumber { get; set; }
        public virtual string CrossReferenceNumber { get; set; }
        public virtual string Company { get; set; }
    }

    public sealed class CrossReferenceMapping : ClassMap<CrossReference>
    {
        public CrossReferenceMapping()
        {
            LazyLoad();
            Table("CrossReference");
            Id(p => p.ID, "CrossReferenceID");
            Map(p => p.BasePartNumber, "BasePartNumber");
            Map(p => p.CrossReferenceNumber, "CrossReferenceNumber");
            Map(m => m.Company, "Company");
        }
    }
}
