﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace CustomerPortalModels
{
    public class CustomerUser : User
    {
        public virtual string DefaultShipTo { get; set; }
        public virtual string AccountManager { get; set; }
        public virtual string SalesRep { get; set; }

        public override string AccountType
        {
            get
            {
                return "Customer";
            }
        }
    }

    public sealed class CustomerUserMapping : ClassMap<CustomerUser>
    {
        public CustomerUserMapping()
        {
            LazyLoad();
            Table("UserCustomer");
            Id(p => p.UserName).GeneratedBy.Assigned();
            Map(p => p.DisplayName);
            Map(p => p.CustomerNumber);
            Map(p => p.DefaultShipTo);
            Map(p => p.AccountManager);
            Map(p => p.SalesRep);
        }
    }
}
