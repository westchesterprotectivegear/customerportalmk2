﻿namespace CustomerPortal.Models
{
    public class CrossReferenceSearchModel
    {
        public string CrossReferenceNumber { get; set; }
        public string Company { get; set; }
    }
}