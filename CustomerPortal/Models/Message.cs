﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CustomerPortal.Models
{
    public class Message
    {
        public string Type { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
    }
}