﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CustomerPortalModels;
using System.ComponentModel.DataAnnotations;

namespace CustomerPortal.Models
{
    public class CreateAccountFromRequestModel
    {
        public string AccountType { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
    }
}