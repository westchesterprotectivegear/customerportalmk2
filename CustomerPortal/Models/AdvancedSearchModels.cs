﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CustomerPortal.Models
{
    public class ProductAdvancedSearchModel
    {
        public string SKU { get; set; }
        public string CustomerSKU { get; set; }
        public string Description { get; set; }
        public double PriceLow { get; set; }
        public double PriceHigh { get; set; }
    }

    public class InvoiceAdvancedSearchModel
    {
        public string InvoiceNumber { get; set; }
        public string PONumber { get; set; }
        public string Status { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public double AmountLow { get; set; }
        public double AmountHigh { get; set; }
    }

    public class OrderAdvancedSearchModel
    {
        public string OrderNumber { get; set; }
        public string PONumber { get; set; }
        public string Status { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public double ValueLow { get; set; }
        public double ValueHigh { get; set; }
    }

    public class UserAdvancedSearchModel
    {
        public string UserName { get; set; }
        public string DisplayName { get; set; }
        public string AccountType { get; set; }
        public string CustomerNumber { get; set; }
    }
}