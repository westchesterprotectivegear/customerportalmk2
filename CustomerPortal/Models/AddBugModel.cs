﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CustomerPortalModels;
using System.ComponentModel.DataAnnotations;

namespace CustomerPortal.Models
{
    public class AddBugModel
    {
        [Required]
        public string Title { get; set; }
    }
}