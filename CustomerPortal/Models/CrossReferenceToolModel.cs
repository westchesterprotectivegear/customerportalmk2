﻿using System.Collections.Generic;
using ProductManagerModels;

namespace CustomerPortal.Models
{
    public class CrossReferenceToolModel
    {
        public List<string> Companies { get; set; }
        public IEnumerable<CrossReference> CrossReferences { get; set; }
    }
}