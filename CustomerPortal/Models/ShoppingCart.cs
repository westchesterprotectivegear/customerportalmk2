﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CustomerPortalModels;

namespace CustomerPortal.Models
{
    public class ShoppingCart
    {
        public virtual int ID { get; set; }
        public virtual string UserName { get; set; }
        public virtual IList<CartLineItem> LineItems { get; set; }

        public virtual int Count
        {
            get
            {
                return (from x in LineItems where !x.Sample select x.Quantity).Sum();
            }
        }

        public virtual int SampleCount
        {
            get
            {
                return (from x in LineItems where x.Sample select x).Count();
            }
        }

        public virtual double Total
        {
            get
            {
                return (from x in LineItems select (x.TotalPrice)).Sum();
            }
        }

        public ShoppingCart()
        {
            LineItems = new List<CartLineItem>();
        }

        public override bool Equals(object obj)
        {
            var cart = (ShoppingCart)obj;

            return UserName == cart.UserName
                && ID == cart.ID
                && Count == cart.Count
                && SampleCount == cart.SampleCount
                && LineItems.Count == cart.LineItems.Count;

            return base.Equals(obj);
        }
    }
}