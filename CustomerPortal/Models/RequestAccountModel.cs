﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace CustomerPortal.Models
{
    public class RequestAccountModel
    {
        [DisplayName("Company Name")]
        [Required]
        public string CompanyName { get; set; }

        [Required]
        [DisplayName("Name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Not a valid email")]
        [DataType(DataType.EmailAddress)]
        [RegularExpression("^[A-Za-z0-9_\\+-]+(\\.[A-Za-z0-9_\\+-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*\\.([A-Za-z]{2,4})$", ErrorMessage = "Not a valid email")]
        [DisplayName("Email address")]
        public string Email { get; set; }

        [Required]
        [DisplayName("Telephone Number")]
        public string TelephoneNumber { get; set; }

        [Required]
        [DisplayName("Street")]
        public string Street { get; set; }
        [Required]
        [DisplayName("City")]
        public string City { get; set; }
        [Required]
        [DisplayName("State")]
        public string State { get; set; }
        [Required]
        [DisplayName("Postal Code")]
        public string Zip { get; set; }

        [DisplayName("Receive Marketing Emails?")]
        public bool ReceiveMarketingEmails { get; set; }
    }
}