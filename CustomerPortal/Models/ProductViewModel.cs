﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CustomerPortalModels;

namespace CustomerPortal.Models
{
    public class ProductViewModel
    {
        public Product Product { get; set; }
        public List<Product> RelatedProducts { get; set; }
        public bool IsFavorite { get; set; }
    }
}