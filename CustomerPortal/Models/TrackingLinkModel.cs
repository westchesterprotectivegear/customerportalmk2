﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CustomerPortal.Models
{
    public class TrackingLinkModel
    {
        public string TrackingNumber { get; set; }
        public string Link { get; set; }
    }
}