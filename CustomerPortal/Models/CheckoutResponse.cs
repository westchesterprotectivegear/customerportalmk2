﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SAPModels;

namespace CustomerPortal.Models
{
    public class CheckoutResponse
    {
        public OrderSubmissionResponse OrderResponse { get; set; }
        public OrderSubmissionResponse SampleResponse { get; set; }
        public Order Order { get; set; }

        public bool Status
        {
            get
            {
                try
                {
                    if (OrderResponse == null)
                        return SampleResponse.Status;
                    else if (SampleResponse == null)
                        return OrderResponse.Status;
                    else
                        return OrderResponse.Status && SampleResponse.Status;
                }
                catch
                {
                    return false;
                }
            }
        }
    }

    public class OrderSubmissionResponse
    {
        public bool Status { get; set; }
        public string Message { get; set; }
        public string OrderNumber { get; set; }
    }
}