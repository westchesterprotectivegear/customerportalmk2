﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace CustomerPortal.Models
{
    public class UsersContext : DbContext
    {
        public UsersContext()
            : base("DefaultConnection")
        {
        }

        public DbSet<UserProfile> UserProfiles { get; set; }
    }

    [Table("UserProfile")]
    public class UserProfile
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }
        public string UserName { get; set; }
    }

    public class RegisterExternalLoginModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        public string ExternalLoginData { get; set; }
    }

    public class LocalPasswordModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class ResetPasswordModel
    {
        public string UserName { get; set; }
        public Guid Key { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class LoginModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class RegisterCustomerModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [Display(Name = "Display name")]
        public string DisplayName { get; set; }

        [Required]
        [Display(Name = "Customer Number")]
        public string CustomerNumber { get; set; }

        [Display(Name = "Default Ship To")]
        public string DefaultShipTo { get; set; }

        [Required]
        [Display(Name = "Account Manager")]
        public string AccountManager { get; set; }

        [Required]
        [Display(Name = "Sales Rep")]
        public string SalesRep { get; set; }

        [Display(Name = "See All Ship Tos")]
        public bool SeeShipTos { get; set; }

        [Display(Name = "Request Samples")]
        public bool RequestSamples { get; set; }

        [Display(Name = "Place Orders")]
        public bool PlaceOrders { get; set; }

        [Display(Name = "Pay Invoices")]
        public bool PayInvoices { get; set; }

        [Display(Name = "See Pricing")]
        public bool SeePricing { get; set; }

        [Display(Name = "View Orders")]
        public bool ViewOrders { get; set; }

        [Display(Name = "Telesales Account")]
        public bool TelesalesAccount { get; set; }

        [Display(Name = "Use Tools")]
        public bool UseTools { get; set; }

        [Display(Name = "Active")]
        public bool Active { get; set; }

        public bool IsRequest { get; set; }
        
        public string Header { get; set; }
    }

    public class EditCustomerModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [Display(Name = "Display name")]
        public string DisplayName { get; set; }

        [Required]
        [Display(Name = "Customer Number")]
        public string CustomerNumber { get; set; }

        [Display(Name = "Default Ship To")]
        public string DefaultShipTo { get; set; }

        [Required]
        [Display(Name = "Account Manager")]
        public string AccountManager { get; set; }

        [Required]
        [Display(Name = "Sales Rep")]
        public string SalesRep { get; set; }

        [Display(Name = "See All Ship Tos")]
        public bool SeeShipTos { get; set; }

        [Display(Name = "Request Samples")]
        public bool RequestSamples { get; set; }

        [Display(Name = "Place Orders")]
        public bool PlaceOrders { get; set; }

        [Display(Name = "Pay Invoices")]
        public bool PayInvoices { get; set; }

        [Display(Name = "See Pricing")]
        public bool SeePricing { get; set; }

        [Display(Name = "View Orders")]
        public bool ViewOrders { get; set; }

        [Display(Name = "Telesales Account")]
        public bool TelesalesAccount { get; set; }

        [Display(Name = "Use Tools")]
        public bool UseTools { get; set; }

        [Display(Name = "Active")]
        public bool Active { get; set; }
    }

    public class RegisterRepGroupModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [Display(Name = "Display name")]
        public string DisplayName { get; set; }

        [Required]
        [Display(Name = "Rep Group Number")]
        public string RepGroupNumber { get; set; }

        [Display(Name = "Default Ship To")]
        public string DefaultShipTo { get; set; }

        [Required]
        [Display(Name = "Account Manager")]
        public string AccountManager { get; set; }

        [Required]
        [Display(Name = "Sales Rep")]
        public string SalesRep { get; set; }

        [Display(Name = "See All Ship Tos")]
        public bool SeeShipTos { get; set; }

        [Display(Name = "Request Samples")]
        public bool RequestSamples { get; set; }

        [Display(Name = "Place Orders")]
        public bool PlaceOrders { get; set; }

        [Display(Name = "Pay Invoices")]
        public bool PayInvoices { get; set; }

        [Display(Name = "See Pricing")]
        public bool SeePricing { get; set; }

        [Display(Name = "View Orders")]
        public bool ViewOrders { get; set; }

        [Display(Name = "Telesales Account")]
        public bool TelesalesAccount { get; set; }

        [Display(Name = "Use Tools")]
        public bool UseTools { get; set; }

        [Display(Name = "Active")]
        public bool Active { get; set; }

        public bool IsRequest { get; set; }

        public string Header { get; set; }
    }

    public class EditRepGroupModel
    {
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [Display(Name = "Display name")]
        public string DisplayName { get; set; }

        [Required]
        [Display(Name = "Rep Group Number")]
        public string RepGroupNumber { get; set; }

        [Display(Name = "Default Ship To")]
        public string DefaultShipTo { get; set; }

        [Required]
        [Display(Name = "Account Manager")]
        public string AccountManager { get; set; }

        [Required]
        [Display(Name = "Sales Rep")]
        public string SalesRep { get; set; }

        [Display(Name = "See All Ship Tos")]
        public bool SeeShipTos { get; set; }

        [Display(Name = "Request Samples")]
        public bool RequestSamples { get; set; }

        [Display(Name = "Place Orders")]
        public bool PlaceOrders { get; set; }

        [Display(Name = "Pay Invoices")]
        public bool PayInvoices { get; set; }

        [Display(Name = "See Pricing")]
        public bool SeePricing { get; set; }

        [Display(Name = "View Orders")]
        public bool ViewOrders { get; set; }

        [Display(Name = "Telesales Account")]
        public bool TelesalesAccount { get; set; }

        [Display(Name = "Use Tools")]
        public bool UseTools { get; set; }

        [Display(Name = "Active")]
        public bool Active { get; set; }
    }

    public class RegisterUserModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [Display(Name = "Display name")]
        public string DisplayName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class EditUserModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [Display(Name = "Display name")]
        public string DisplayName { get; set; }

        [Display(Name = "See All Ship Tos")]
        public bool SeeShipTos { get; set; }

        [Display(Name = "Request Samples")]
        public bool RequestSamples { get; set; }

        [Display(Name = "Place Orders")]
        public bool PlaceOrders { get; set; }

        [Display(Name = "Pay Invoices")]
        public bool PayInvoices { get; set; }

        [Display(Name = "See Pricing")]
        public bool SeePricing { get; set; }

        [Display(Name = "View Orders")]
        public bool ViewOrders { get; set; }

        [Display(Name = "Use Tools")]
        public bool UseTools { get; set; }

        [Display(Name = "Create Users")]
        public bool CreateUsers { get; set; }

        [Display(Name = "Active")]
        public bool Active { get; set; }
    }

    public class ExternalLogin
    {
        public string Provider { get; set; }
        public string ProviderDisplayName { get; set; }
        public string ProviderUserId { get; set; }
    }
}
