﻿namespace CustomerPortal.Models
{
    public class ShipToSearchModel
    {
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string CustomerNumber { get; set; }
    }
}