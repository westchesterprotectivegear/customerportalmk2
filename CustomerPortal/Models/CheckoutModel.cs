﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;

namespace CustomerPortal.Models
{
    public class CheckoutModel
    {
        [Required]
        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }

        [Display(Name = "Attention")]
        public string Attention { get; set; }

        [Required]
        [Display(Name = "PO Number")]
        public string PONumber { get; set; }

        [Required]
        [Display(Name = "Customer PO Number")]
        public string CustomerPONumber { get; set; }

        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }

        [Display(Name = "Payment Terms")]
        public string PaymentTerms { get; set; }

        [Display(Name = "Carrier")]
        public string Carrier { get; set; }

        [Display(Name = "Account Number")]
        public string AccountNumber { get; set; }

        [Display(Name = "Shipping Instructions")]
        public string ShippingInstructions { get; set; }

        public bool Telesales { get; set; }

        [Display(Name = "Ship To")]
        public string ShipToID { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string Zip { get; set; }
        public string Name { get; set; }
        [Display(Name = "Notify me when my order ships")]
        public bool sendNotification { get; set; }
        [Display(Name = "Send Notification To: ")]
        public string NotificationEmail { get; set; }

        public ShoppingCart Cart { get; set; }
    }
}
