﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;

namespace CustomerPortal.Models
{
    public class CheckoutStep2
    {
        [Required]
        [Display(Name = "Ship To")]
        public string ShipToID { get; set; }

        [Required]
        public string State { get; set; }

        [Required]
        public string City { get; set; }

        [Required]
        public string Street { get; set; }

        [Required]
        public string Zip { get; set; }

        [Required]
        public string Name { get; set; }
		
		[Required]
        [Display(Name = "Request as Permanent Shipping Address")]
        public bool RequestPermanent { get; set; }
       
    }
}
