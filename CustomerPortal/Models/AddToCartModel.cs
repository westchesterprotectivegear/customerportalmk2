﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CustomerPortalModels;
using System.ComponentModel.DataAnnotations;

namespace CustomerPortal.Models
{
    public class AddToCartModel
    {
        public int Quantity { get; set; }
        public string SKU { get; set; }
        public Guid ProductID { get; set; }
    }
}