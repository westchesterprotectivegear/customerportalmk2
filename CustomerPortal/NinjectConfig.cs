﻿using System;
using Ninject;
using System.Web.Mvc;
using System.Web.Routing;
using Ninject.Web.Common;
using Ninject.Extensions.Conventions;

namespace CustomerPortal
{
    public class NinjectConfig
    {
        private static IKernel _ninjectKernel;
        public class NinjectDependencyResolver : DefaultControllerFactory
        {
            public NinjectDependencyResolver()
            {
                _ninjectKernel = new StandardKernel();

                ConfigureDependency();
            }
            protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
            {
                return controllerType == null ? null : (IController)_ninjectKernel.Get(controllerType);
            }
        }
        private static void ConfigureDependency()
        {
            _ninjectKernel.Bind<IUnitOfWork<CustomerPortalDatabaseContext>>().To<UnitOfWork<CustomerPortalDatabaseContext>>().InRequestScope();
            _ninjectKernel.Bind(x => x.FromAssembliesMatching(new string[]{ "CustomerPortalModels.dll", "CustomerPortal.dll", "SAPModels.dll" }).SelectAllClasses().Excluding<UnitOfWork<CustomerPortalDatabaseContext>>().BindDefaultInterface());
        }
    }
}