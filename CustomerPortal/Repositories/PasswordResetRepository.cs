﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CustomerPortal.Models;
using NHibernate;
using NHibernate.Linq;
using NCommon.Specifications;
using CustomerPortalModels;

namespace CustomerPortal
{
    public interface IPasswordResetRepository : IRepository
    {
        void Add(PasswordReset reset);
        PasswordReset Get(Guid id);
        void Remove(PasswordReset reset);
    }

    public class PasswordResetRepository : IPasswordResetRepository
    {
        private readonly ISession _session;
        public PasswordResetRepository()
        {
            _session = CustomerPortalSessionFactory.GetCurrentSession();
        }
        public void Add(PasswordReset reset)
        {
            using (var transaction = _session.BeginTransaction())
            {
                _session.SaveOrUpdate(reset);
                transaction.Commit();
            }
        }

        public PasswordReset Get(Guid key)
        {
            return _session.Query<PasswordReset>().First(x => x.PrivateKey == key);
        }

        public void Remove(PasswordReset reset)
        {
            using (var transaction = _session.BeginTransaction())
            {
                _session.Delete(reset);
                transaction.Commit();
            }
        }
    }
}