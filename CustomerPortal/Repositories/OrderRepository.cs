﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CustomerPortal.Models;
using NHibernate;
using NHibernate.Linq;
using NCommon.Specifications;
using SAPModels;

namespace CustomerPortal
{
    public interface IOrderRepository : IRepository
    {
        Order Get(string orderNumber, string customerNumber);
        IEnumerable<Order> GetAll(string customerNumber);
        bool OrderExists(string customerNumber, string poNumber);
        IEnumerable<Order> GetAll(Specification<Order> spec);
    }

    public class OrderRepository : IOrderRepository
    {
        private readonly ISession _session;
        public OrderRepository()
        {
            _session = SAPSessionFactory.GetCurrentSession();
        }

        public Order Get(string orderNumber, string customerNumber)
        {
            return _session.Query<Order>().First(x => x.SoldTo == customerNumber && x.SalesOrder == orderNumber.PadSAP(10));
        }

        public IEnumerable<Order> GetAll(string customerNumber)
        {
            return _session.Query<Order>().Where(x => x.SoldTo == customerNumber.PadSAP(10));
        }

        public bool OrderExists(string customerNumber, string poNumber)
        {
            return _session.Query<Order>().Any(x => x.SoldTo == customerNumber.PadSAP(10) && x.PurchaseOrder == poNumber.PadSAP(10));
        }

        public IEnumerable<Order> GetAll(Specification<Order> spec)
        {
            return _session.Query<Order>().Where(spec.Predicate).Take(100);
        }
    }
}