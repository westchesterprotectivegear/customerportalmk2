﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CustomerPortal.Models;
using NHibernate;
using NHibernate.Linq;
using NCommon.Specifications;
using ProductManagerModels;

namespace CustomerPortal
{
    public interface ICrossReferenceRepository : IRepository
    {
        List<string> GetCompanies();
        IEnumerable<CrossReference> GetAll(CrossReferenceSearchModel model);
    }

    public class CrossReferenceRepository : ICrossReferenceRepository
    {
        private readonly ISession _session;
        public CrossReferenceRepository()
        {
            _session = CustomerPortalSessionFactory.GetCurrentSession();
        }

        public List<string> GetCompanies()
        {
            return (from x in _session.Query<CrossReference>() select x.Company).Distinct().ToList();
        }

        public IEnumerable<CrossReference> GetAll(CrossReferenceSearchModel model)
        {
            var xrefs = _session.Query<CrossReference>();

            if (!string.IsNullOrEmpty(model.CrossReferenceNumber))
                xrefs = xrefs.Where(x => x.CrossReferenceNumber == model.CrossReferenceNumber);

            if (!string.IsNullOrEmpty(model.Company))
                xrefs = xrefs.Where(x => x.Company == model.Company);

            return xrefs;
        }
    }
}