﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CustomerPortal.Models;
using NHibernate;
using NHibernate.Linq;
using NCommon.Specifications;
using CustomerPortalModels;

namespace CustomerPortal
{
    public interface IBugCommentRepository : IRepository
    {
        void Add(BugComment comment);
        BugComment Get(Guid id);
        void Remove(BugComment comment);
    }

    public class BugCommentRepository : IBugCommentRepository
    {
        private readonly ISession _session;
        public BugCommentRepository()
        {
            _session = CustomerPortalSessionFactory.GetCurrentSession();
        }

        public void Add(BugComment comment)
        {
            using (var transaction = _session.BeginTransaction())
            {
                _session.Save(comment);
                transaction.Commit();
            }
        }

        public BugComment Get(Guid id)
        {
            return _session.Query<BugComment>().First(x => x.ID == id);
        }

        public void Remove(BugComment comment)
        {
            using (var transaction = _session.BeginTransaction())
            {
                _session.Delete(comment);
                transaction.Commit();
            }
        }
    }
}