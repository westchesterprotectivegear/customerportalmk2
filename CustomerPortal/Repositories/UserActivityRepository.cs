﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CustomerPortal.Models;
using NHibernate;
using NHibernate.Linq;
using CustomerPortalModels;

namespace CustomerPortal
{
    public interface IUserActivityRepository : IRepository
    {
        void Add(UserActivity userActivity);
        UserActivity Get(int id);
        IEnumerable<UserActivity> GetAll();
        IEnumerable<UserActivity> GetAll(string user);
    }

    public class UserActivityRepository : IUserActivityRepository
    {
        private readonly ISession _session;

        public UserActivityRepository()
        {
            _session = CustomerPortalSessionFactory.GetCurrentSession();
        }

        public void Add(UserActivity userActivity)
        {
            using (var transaction = _session.BeginTransaction())
            {
                _session.Save(userActivity);
                transaction.Commit();
            }
        }

        public UserActivity Get(int id)
        {
            return CustomerPortalSessionFactory.GetCurrentSession().Get<UserActivity>(id);
        }

        public IEnumerable<UserActivity> GetAll()
        {
            return _session.Query<UserActivity>();
        }

        public IEnumerable<UserActivity> GetAll(string user)
        {
            return _session.Query<UserActivity>().Where(x => x.User == user);
        }
    }
}