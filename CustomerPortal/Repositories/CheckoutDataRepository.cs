﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CustomerPortal.Models;
using NHibernate;
using NHibernate.Linq;
using NCommon.Specifications;
using CustomerPortalModels;

namespace CustomerPortal
{
    public interface ICheckoutDataRepository : IRepository
    {
        void Add(CheckoutData data);
        void Save(CheckoutData data);
        void Remove(CheckoutData data);
        CheckoutData Get(Guid id);
        IEnumerable<CheckoutData> GetAll();
    }

    public class CheckoutDataRepository : ICheckoutDataRepository
    {
        private readonly ISession _session;
        public CheckoutDataRepository()
        {
            _session = CustomerPortalSessionFactory.GetCurrentSession();
        }

        public void Add(CheckoutData data)
        {
            using (var transaction = _session.BeginTransaction())
            {
                _session.Save(data);
                transaction.Commit();
            }
        }

        public void Save(CheckoutData data)
        {
            using (var transaction = _session.BeginTransaction())
            {
                _session.SaveOrUpdate(data);
                transaction.Commit();
            }
        }

        public void Remove(CheckoutData data)
        {
            using (var transaction = _session.BeginTransaction())
            {
                _session.Delete(data);
                transaction.Commit();
            }
        }

        public CheckoutData Get(Guid id)
        {
            return _session.Query<CheckoutData>().FirstOrDefault(x => x.ID == id);
        }

        public IEnumerable<CheckoutData> GetAll()
        {
            return _session.Query<CheckoutData>();
        }
    }
}