﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CustomerPortal.Models;
using NHibernate;
using NHibernate.Linq;
using NCommon.Specifications;
using SAPModels;
using System.Linq.Expressions;	  

namespace CustomerPortal
{
    public interface IShipToRepository : IRepository
    {
        ShipTo Get(string shipToNumber);
        IEnumerable<ShipTo> GetAll(string customerNumber);
		IQueryable<ShipTo> GetGroup(Expression<Func<ShipTo, bool>> predicate);
    }

    public class ShipToRepository : IShipToRepository
    {
        private readonly ISession _session;
        public ShipToRepository()
        {
            _session = SAPSessionFactory.GetCurrentSession();
        }

        public ShipTo Get(string shipToNumber)
        {
            return _session.Query<ShipTo>().First(x => x.ID == shipToNumber);
        }

        public IEnumerable<ShipTo> GetAll(string customerNumber)
        {
            return _session.Query<ShipTo>().Where(x => x.ID.StartsWith(customerNumber));
        }

        public IQueryable<ShipTo> GetGroup(Expression<Func<ShipTo, bool>> predicate)
        {
            return _session.Query<ShipTo>().Where(predicate);
        }
    }
}