﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CustomerPortal.Models;
using NHibernate;
using NHibernate.Linq;
using NCommon.Specifications;
using CustomerPortalModels;

namespace CustomerPortal
{
    public interface IProductRepository : IRepository
    {
        void Add(Product product);
        Product Get(Guid id);
        Product Get(string sku, string customerNumber);
        bool Exists(string sku, string customerNumber);
        void Remove(Product product);
        IEnumerable<Product> GetAll();
        IEnumerable<Product> GetAll(string customerNumber);
        IEnumerable<Product> GetAll(IEnumerable<string> skus, string customerNumber);
    }

    public class ProductRepository : IProductRepository
    {
        private readonly ISession _session;

        public ProductRepository()
        {
            _session = CustomerPortalSessionFactory.GetCurrentSession();
        }

        public void Add(Product product)
        {
            _session.Save(product);
        }

        public Product Get(Guid id)
        {
            return _session.Query<Product>().FirstOrDefault(x => x.ID == id);
        }

        public Product Get(string sku, string customerNumber)
        {
            return _session.Query<Product>().FirstOrDefault(x => x.SKU == sku && x.CustomerNumber == customerNumber);
        }

        public bool Exists(string sku, string customerNumber)
        {
            return _session.Query<Product>().Any(x => x.SKU == sku && x.CustomerNumber == customerNumber);
        }

        public void Remove(Product product)
        {
            _session.Delete(product);
        }

        public IEnumerable<Product> GetAll()
        {
            return _session.Query<Product>();
        }

        public IEnumerable<Product> GetAll(string customerNumber)
        {
            return _session.Query<Product>().Where(x => x.CustomerNumber == customerNumber);
        }

        public IEnumerable<Product> GetAll(IEnumerable<string> skus, string customerNumber)
        {
            return _session.Query<Product>().Where(x => x.CustomerNumber == customerNumber && skus.Contains(x.SKU));
        }
    }
}