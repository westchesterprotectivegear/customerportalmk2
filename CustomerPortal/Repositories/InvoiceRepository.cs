﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CustomerPortal.Models;
using NHibernate;
using NHibernate.Linq;
using NCommon.Specifications;
using SAPModels;

namespace CustomerPortal
{
    public interface IInvoiceRepository : IRepository
    {
        Invoice Get(string poNumber);
        IEnumerable<Invoice> GetAll();
        IEnumerable<Invoice> GetAll(string customerNumber);
        IEnumerable<Invoice> GetAll(Specification<Invoice> spec);
    }

    public class InvoiceRepository : IInvoiceRepository
    {
        private readonly ISession _session;
        public InvoiceRepository()
        {
            _session = SAPSessionFactory.GetCurrentSession();
        }

        public Invoice Get(string poNumber)
        {
            return CustomerPortalSessionFactory.GetCurrentSession().Query<Invoice>().First(x => x.PONumber == poNumber);
        }

        public IEnumerable<Invoice> GetAll()
        {
            return _session.Query<Invoice>();
        }

        public IEnumerable<Invoice> GetAll(string customerNumber)
        {
            if (customerNumber == "LY02")
                return _session.Query<Invoice>().Where(x => x.CustomerNumber == customerNumber.PadSAP(10) && x.PONumber.StartsWith("500")).Take(100);

            return _session.Query<Invoice>().Where(x => x.CustomerNumber == customerNumber.PadSAP(10)).Take(100);
        }

        public IEnumerable<Invoice> GetAll(Specification<Invoice> spec)
        {
            return _session.Query<Invoice>().Where(spec.Predicate).Take(100);
        }
    }
}