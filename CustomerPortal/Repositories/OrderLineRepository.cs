﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CustomerPortal.Models;
using NHibernate;
using NHibernate.Linq;
using NCommon.Specifications;
using SAPModels;

namespace CustomerPortal
{
    public interface IOrderLineRepository : IRepository
    {
        IEnumerable<OrderLine> Get(string orderNumber);
        IEnumerable<OrderLine> GetAll();
    }

    public class OrderLineRepository : IOrderLineRepository
    {
        private readonly ISession _session;
        public OrderLineRepository()
        {
            _session = SAPSessionFactory.GetCurrentSession();
        }

        public IEnumerable<OrderLine> Get(string orderNumber)
        {
            return _session.Query<OrderLine>().Where(x => x.OrderNumber == orderNumber.PadSAP(10));
        }

        public IEnumerable<OrderLine> GetAll()
        {
            return _session.Query<OrderLine>();
        }
    }
}