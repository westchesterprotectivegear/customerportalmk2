﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CustomerPortal.Models;
using NHibernate;
using NHibernate.Linq;
using NCommon.Specifications;
using SAPModels;
using MoreLinq;

namespace CustomerPortal
{
    public interface ICustomerRepository : IRepository
    {
        Customer Get(string customerNumber);
        IEnumerable<Customer> GetAll();
        IEnumerable<Customer> GetAll(string repGroup);
    }

    public class CustomerRepository : ICustomerRepository
    {
        private readonly ISession _session;
        public CustomerRepository()
        {
            _session = SAPSessionFactory.GetCurrentSession();
        }

        public Customer Get(string customerNumber)
        {
            return _session.Query<Customer>().FirstOrDefault(x => x.Number == customerNumber);
        }

        public IEnumerable<Customer> GetAll()
        {
            return _session.Query<Customer>();
        }

        public IEnumerable<Customer> GetAll(string repGroup)
        {
            return _session.Query<Customer>().Where(x => x.RepGroup == repGroup).DistinctBy(x => x.ParentNumber);
        }
    }
}