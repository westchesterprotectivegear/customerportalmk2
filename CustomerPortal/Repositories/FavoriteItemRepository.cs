﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CustomerPortal.Models;
using NHibernate;
using NHibernate.Linq;
using NCommon.Specifications;
using CustomerPortalModels;

namespace CustomerPortal
{
    public interface IFavoriteItemRepository : IRepository
    {
        void Add(FavoriteItem favoriteItem);
        void Remove(string sku, string userName);
        bool Exists(string sku, string userName);
        IEnumerable<FavoriteItem> GetAll(string userName);
    }

    public class FavoriteItemRepository : IFavoriteItemRepository
    {
        private readonly ISession _session;
        public FavoriteItemRepository()
        {
            _session = CustomerPortalSessionFactory.GetCurrentSession();
        }
        public void Add(FavoriteItem favoriteItem)
        {
            using (var transaction = _session.BeginTransaction())
            {
                _session.Save(favoriteItem);
                transaction.Commit();
            }
        }

        public void Remove(string sku, string userName)
        {
            using (var transaction = _session.BeginTransaction())
            {
                var fav = _session.Query<FavoriteItem>().First(x => x.SKU == sku && x.UserName == userName);
                _session.Delete(fav);
                transaction.Commit();
            }
        }

        public bool Exists(string sku, string userName)
        {
            return _session.Query<FavoriteItem>().Any(x => x.SKU == sku && x.UserName == userName);
        }

        public IEnumerable<FavoriteItem> GetAll(string userName)
        {
            return _session.Query<FavoriteItem>().Where(x => x.UserName == userName);
        }
    }
}