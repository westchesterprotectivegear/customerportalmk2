﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CustomerPortal.Models;
using NHibernate;
using NHibernate.Linq;
using NCommon.Specifications;
using CustomerPortalModels;

namespace CustomerPortal
{
    public interface IAccountRequestRepository : IRepository
    {
        void Add(AccountRequest accountRequest);
        void Save(AccountRequest accountRequest);
        void Remove(AccountRequest accountRequest);
        bool Exists(string email);
        AccountRequest Get(string email);
        IEnumerable<AccountRequest> GetAll();
        IEnumerable<AccountRequest> GetAll(string company);
    }

    public class AccountRequestRepository : IAccountRequestRepository
    {
        private readonly ISession _session;
        public AccountRequestRepository()
        {
            _session = CustomerPortalSessionFactory.GetCurrentSession();
        }
        public void Add(AccountRequest accountRequest)
        {
            using (var transaction = _session.BeginTransaction())
            {
                _session.SaveOrUpdate(accountRequest);
                transaction.Commit();
            }
        }

        public void Save(AccountRequest accountRequest)
        {
            using (var transaction = _session.BeginTransaction())
            {
                _session.SaveOrUpdate(accountRequest);
                transaction.Commit();
            }
        }

        public void Remove(AccountRequest accountRequest)
        {
            CustomerPortalSessionFactory.GetCurrentSession().Delete(accountRequest);
        }

        public bool Exists(string email)
        {
            return CustomerPortalSessionFactory.GetCurrentSession().Query<AccountRequest>().Any(x => x.Email == email);
        }

        public AccountRequest Get(string email)
        {
            return CustomerPortalSessionFactory.GetCurrentSession().Query<AccountRequest>().FirstOrDefault(x => x.Email == email);
        }

        public IEnumerable<AccountRequest> GetAll()
        {
            return _session.Query<AccountRequest>();
        }

        public IEnumerable<AccountRequest> GetAll(string company)
        {
            return _session.Query<AccountRequest>().Where(x => x.Company == company);
        }
    }
}