﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CustomerPortal.Models;
using NHibernate;
using NHibernate.Linq;
using NCommon.Specifications;
using CustomerPortalModels;

namespace CustomerPortal
{
    public interface IShoppingCartRepository : IRepository
    {
        ShoppingCart Get(string userName);
        void AddItemToCart(string sku, int quantity, string userName, bool sample);
        void RemoveItemFromCart(string sku, string userName);
        void UpdateLineItem(string sku, int quantity, string userName);
        void ClearCart(string userName);
    }

    public class ShoppingCartRepository : IShoppingCartRepository
    {
        private readonly ISession _session;

        public ShoppingCartRepository()
        {
            _session = CustomerPortalSessionFactory.GetCurrentSession();
        }

        public ShoppingCart Get(string userName)
        {
            var cart = new ShoppingCart()
            {
                UserName = userName,
                LineItems = _session.Query<CartLineItem>().Where(x => x.UserName == userName).ToList()
            };

            return cart;
        }

        public void AddItemToCart(string sku, int quantity, string userName, bool sample)
        {
            var existingLine = _session.Query<CartLineItem>().FirstOrDefault(x => x.SKU == sku && x.UserName == userName && x.Sample == sample);

            if (existingLine != null)
                UpdateLineItem(sku, quantity + existingLine.Quantity, userName);
            else
            {

                CartLineItem lineItem = new CartLineItem()
                {
                    Quantity = quantity,
                    SKU = sku,
                    UserName = userName,
                    Sample = sample
                };
                using (var transaction = _session.BeginTransaction())
                {
                    _session.Save(lineItem);
                    transaction.Commit();
                }
            }
        }

        public void UpdateLineItem(string sku, int quantity, string userName)
        {
            var lineItem = _session.Query<CartLineItem>().First(x => x.SKU == sku && x.UserName == userName);
            lineItem.Quantity = quantity;

            using (var transaction = _session.BeginTransaction())
            {
                if (lineItem.Quantity > 0)
                    _session.Save(lineItem);
                else
                    _session.Delete(lineItem);
                transaction.Commit();
            }
        }

        public void RemoveItemFromCart(string sku, string userName)
        {
            using (var transaction = _session.BeginTransaction())
            {
                var line = _session.Query<CartLineItem>().First(x => x.UserName == userName && x.SKU == sku);
                _session.Delete(line);
                transaction.Commit();
            }
        }

        public void ClearCart(string userName)
        {
            using (var transaction = _session.BeginTransaction())
            {
                foreach (var line in Get(userName).LineItems)
                    _session.Delete(line);
                transaction.Commit();
            }
        }
    }
}