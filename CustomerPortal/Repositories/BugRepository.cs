﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CustomerPortal.Models;
using NHibernate;
using NHibernate.Linq;
using NCommon.Specifications;
using CustomerPortalModels;

namespace CustomerPortal
{
    public interface IBugRepository : IRepository
    {
        void Add(Bug bug);
        Bug Get(Guid id);
        void Remove(Bug bug);
        void Open(Guid id);
        void Close(Guid id);
        IEnumerable<Bug> GetAll();
        IEnumerable<Bug> GetAll(string userName);
        IEnumerable<Bug> GetAllOpen();
        IEnumerable<Bug> GetAll(Specification<Bug> spec);
    }

    public class BugRepository : IBugRepository
    {
        private readonly ISession _session;
        public BugRepository()
        {
            _session = CustomerPortalSessionFactory.GetCurrentSession();
        }
        public void Add(Bug bug)
        {
            using (var transaction = _session.BeginTransaction())
            {
                _session.Save(bug);
                transaction.Commit();
            }
        }

        public Bug Get(Guid id)
        {
            return _session.Query<Bug>().First(x => x.ID == id);
        }

        public void Remove(Bug bug)
        {
            using (var transaction = _session.BeginTransaction())
            {
                _session.Delete(bug);
                transaction.Commit();
            }
        }

        public void Open(Guid id)
        {
            var bug = Get(id);
            bug.Status = "Open";
            using (var transaction = _session.BeginTransaction())
            {
                _session.SaveOrUpdate(bug);
                transaction.Commit();
            }
        }

        public void Close(Guid id)
        {
            var bug = Get(id);
            bug.Status = "Closed";
            using (var transaction = _session.BeginTransaction())
            {
                _session.SaveOrUpdate(bug);
                transaction.Commit();
            }
        }

        public IEnumerable<Bug> GetAll()
        {
            return _session.Query<Bug>();
        }

        public IEnumerable<Bug> GetAll(string userName)
        {
            return _session.Query<Bug>().Where(x => x.UserName == userName && x.Status == "Open");
        }

        public IEnumerable<Bug> GetAllOpen()
        {
            return _session.Query<Bug>().Where(x => x.Status == "Open");
        }

        public IEnumerable<Bug> GetAll(Specification<Bug> spec)
        {
            return _session.Query<Bug>().Where(spec.Predicate).Take(100);
        }
    }
}