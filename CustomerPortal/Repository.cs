﻿using System;
using System.Diagnostics;
using System.Linq;
using NCommon.Specifications;
using NHibernate;
using NHibernate.Linq;

namespace CustomerPortal
{
    public interface IRepository<TEntity, TContext> where TEntity : class where TContext : DatabaseContext
    {
        IQueryable<TEntity> GetAll();
        TEntity Get(Guid id);
        TEntity Get(string id);
        TEntity Get(int id);
        TEntity Get(Specification<TEntity> spec);
        void Create(TEntity entity);
        void Update(TEntity entity);
        void Delete(Guid id);
        void Delete(string id);
        void Delete(int id);
        void Delete(TEntity entity);
    }

    public class Repository<TEntity, TContext> : IRepository<TEntity, TContext> where TEntity : class where TContext : DatabaseContext
    {
        private UnitOfWork<TContext> _unitOfWork;
        public Repository(IUnitOfWork<TContext> unitOfWork)
        {
            _unitOfWork = (UnitOfWork<TContext>)unitOfWork;
        }

        protected ISession Session { get { return _unitOfWork.Session; } }

        public IQueryable<TEntity> GetAll()
        {
            return Session.Query<TEntity>();
        }

        public TEntity Get(Guid id)
        {
            return Session.Get<TEntity>(id);
        }

        public TEntity Get(string id)
        {
            return Session.Get<TEntity>(id);
        }

        public TEntity Get(int id)
        {
            return Session.Get<TEntity>(id);
        }

        public TEntity Get(Specification<TEntity> spec)
        {
            return Session.Query<TEntity>().First(spec.Predicate);
        }

        public void Create(TEntity entity)
        {
            Session.Save(entity);
        }

        public void Update(TEntity entity)
        {
            Session.Update(entity);
        }

        public void Delete(Guid id)
        {
            Session.Delete(Session.Load<TEntity>(id));
        }

        public void Delete(string id)
        {
            Session.Delete(Session.Load<TEntity>(id));
        }

        public void Delete(int id)
        {
            Session.Delete(Session.Load<TEntity>(id));
        }

        public void Delete(TEntity entity)
        {
            Session.Delete(entity);
        }
    }
}