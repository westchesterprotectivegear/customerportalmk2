﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CustomerPortal
{
    public static class TrackingFactory
    {
        public static string GetLink(string trackingNumber, string carrier)
        {
            string link = "";

            switch (carrier)
            {
                case "AA01.1":           //AAA Cooper Transportation
                    link = "http://www.aaacooper.com/ProTrackResults.aspx?ProNum=" + trackingNumber + "&AllAccounts=true";
                    break;

                case "A07.1":            //ABF Freight System"
                    //Needs Checking
                    link = "https://www.abfs.com/tools/trace/default.asp?hidSubmitted=Y&refno0=" + trackingNumber + "&reftype0=A";
                    break;

                case "AB01.1":           //Aberdeen Express
                    link = "http://www.aberdeenexpress.com/";
                    break;

                case "AL03.1":           //Allstates Freight Link
                    link = "http://www.allstatesfreight.com/";
                    break;

                case "AI01":             //Airborbe express now DHL
                    link = "http://track.dhl-usa.com/TrackByNbr.asp?ShipmentNumber=" + trackingNumber;
                    break;

                case "AL02.1":           //Alvan Motor Freight - Out of Business
                    link = "";
                    break;

                case "AV01.1":           //Averitt Express
                    link = "https://www.averittexpress.com/trackLTLById.avrt?serviceType=LTL&resultsPageTitle=LTL+Tracking+by+PRO+and+BOL&trackPro=" + trackingNumber;
                    break;

                case "BA01.1":           //DB Schenker
                    link = "http://www.dbschenkerusa.com/";
                    break;

                case "BE01.1":           //DB Schenker
                    link = "https://bjbf.loadtracking.com/BJBFOnlineTools/GetDetailUsingProNumber?proNumber=" + trackingNumber;
                    break;
                    
                case "CR01.1":           //Cross Country Cartage
                    link = "http://www.crosscountycartage.com/";
                    break;

                case "CA03.1":           //caseStack
                    link = "";
                    break;

                case "CO07.1":           //Con-Way Transportation
                    link = "https://www.con-way.com/webapp/manifestrpts_p_app/Tracking/TrackingRS.jsp?refNbr1=" + trackingNumber.Replace("-", "");
                    break;

                case "TH01.1":           //The Connection Co.
                    link = "";
                    break;

                case "CE01.1":           //Central Transport
                    link = "http://www.centraltransportint.com/Confirm/trace.aspx?pro=" + trackingNumber;
                    break;

                case "DA03.1":           //Dayton Freight Lines
                    link = "http://www.daytonfreight.com/Tracking/TrackingDetail.aspx?proNum=" + trackingNumber;
                    break;

                case "DA01.1":           //Roadrunner Dawes Freight Sys
                    link = "http://www.rrts.com/tools/tracing/TraceMultipleResults.aspx?PROS=" + trackingNumber;
                    break;

                case "D01":              //DHL Express (USA)
                    link = "http://track.dhl-usa.com/TrackByNbr.asp?ShipmentNumber=" + trackingNumber;
                    break;

                case "DI01.1":           //Direct-X
                    link = "http://www.direct-x.org/";
                    break;

                case "ES01.1":           //Estes Express Lines
                    link = "http://www.estes-express.com/cgi-dta/edn419.mbr/output?search_criteria=" + trackingNumber;
                    break;

                case "FE01":             //FEDEX
                    link = "http://www.fedex.com/Tracking?action=track&language=english&cntry_code=us&initial=x&tracknumbers=" + trackingNumber;
                    break;

                case "AM04.1":           //FEDEX Freight
                    link = "http://www.fedex.com/Tracking?action=track&language=english&cntry_code=us&initial=x&tracknumbers=" + trackingNumber;
                    break;

                case "WA02.1":           //Fedex National LTL
                    link = "http://www.fedex.com/Tracking?action=track&language=english&cntry_code=us&initial=x&tracknumbers=" + trackingNumber;
                    break;

                case "U02.1":            //USF Holland
                    //Needs Checking
                    //s = "http://www.yrcregional.com/hollandregional/index.shtml"
                    link = "https://my.yrcregional.com/XMLServices/REST/v2/x1/doTrackDetail?searchBy=PRO&number=" + trackingNumber + "&output=HTML";
                    break;

                case "J01.1":            //J + J Transportaion
                    link = "http://www.jjfreight.com/index.html";
                    break;

                case "KI03.1":           //Kingsgate Transportation
                    link = "http://www.kingsgatetrans.com/";
                    break;

                case "KI02.1":           //King//s Transfer
                    //Needs more investigation
                    link = "";
                    break;

                case "LO02.1":           //Load Delivered Logistics
                    link = "http://www.loaddelivered.com/services.htm";
                    break;

                case "MU02.1":           //Multi-Serv Transportation
                    link = "http://www.multiservtransportation.com/";
                    break;

                case "MI02.1":           //Milan Express
                    link = "http://www.milanexpress.com/dynamic/shiptrac.asp?txtProNo=" + trackingNumber.Replace("-", "");
                    break;

                case "M02.1":            //M&M American
                    link = "http://www.mm-american.com/Home/tabid/195/Default.aspx";
                    break;

                case "MI01.1":           //Mid-States
                    link = "http://www.midstatesexpress.com/";
                    break;

                case "MU01.1":          //Murphy Transportation
                    link = "";
                    break;

                case "NE02.1":           //New Enland Motor Freight
                    link = "http://nemfweb.nemf.com/shptrack.nsf/request?openagent=1&pro=" + trackingNumber + "&x=20&y=9";
                    break;

                case "N01.1":            //NYK Logistics
                    link = "";
                    break;

                case "OL02":             //Old Dominion Freight Lines
                    link = "http://www.odfl.com/trace/Trace.jsp?pronum=" + trackingNumber;
                    break;

                case "OV01.1":        //Overnite
                    link = "";
                    break;

                case "PI02":             //Pitney Bowes
                    link = "";
                    break;

                case "PI03.1":           //Pitt Ohio Express
                    link = "http://works.pittohio.com/mypittohio/OnlineTools/OT_ShipmentTrace_Details.asp?sessionid=52BC18C8-FD50-49A0-BC60-AADE06AAE610&menuid=&pro=" + trackingNumber + "&bol=&PP=0";
                    break;

                case "PI03.2":           //Pitt Ohio Express
                    link = "http://works.pittohio.com/mypittohio/OnlineTools/OT_ShipmentTrace_Details.asp?sessionid=52BC18C8-FD50-49A0-BC60-AADE06AAE610&menuid=&pro=" + trackingNumber + "&bol=&PP=0";
                    break;

                case "PJ01.1":           //PJAX
                    link = "http://www.vitranexpress.com/tracing/Tracing2.aspx?epr=" + trackingNumber;
                    break;

                case "RO03.1":           //Roadrunner
                    link = "http://www.rrts.com/tools/tracing/TraceMultipleResults.aspx?PROS=" + trackingNumber;
                    break;

                case "RO01.1":           //YRC Worldwide
                    link = "http://my.yrc.com/dynamic/national/servlet?CONTROLLER=com.rdwy.ec.rextracking.http.controller.ProcessPublicTrackingController&PRONumber=" + trackingNumber.Replace("-", "");
                    break;

                case "R01.1":            //R&L Carriers
                    link = "http://www.rlcarriers.com/shiptrace2.asp?traceseek=PRO&tracenum=" + trackingNumber.Replace("-", "");
                    break;

                case "RU01.1":           //Rush Transportation/Logistics
                    link = "http://www.rush-delivery.com/index.html";
                    break;

                case "SA01.1":           //SAIA Motor Freight Line
                    link = "http://www.saia.com/V2/tracing/mnf2.aspx?m=2&UID=&PWD=&SID=KOLUW26106035&PRONum1=" + trackingNumber.Replace("-", "");
                    break;

                case "U04.1":            //UPS Feight
                    link = "http://ltl.upsfreight.com/tracking/ShipTrack.aspx?ProNbr=" + trackingNumber;
                    break;

                case "U01.1":            //Dugan Truck Line
                    link = "http://www.dugantruckline.com/";
                    break;

                case "UN01":             //United Parcel Service (UPS)
                    link = "http://wwwapps.ups.com/etracking/tracking.cgi?tracknums_" +
                        "displayed=5&TypeOfInquiryNumber=T&HTMLVersion=4.0&" +
                        "InquiryNumber1=" + trackingNumber +
                        "&track=Track";
                    break;

                case "VI01.1":           //Vitran Express
                    link = "http://www.vitranexpress.com/tracing/Tracing2.aspx?epr=" + trackingNumber;
                    break;

                case "OV02.1":           //Vitran Express
                    link = "http://www.vitranexpress.com/tracing/Tracing2.aspx?epr=" + trackingNumber;
                    break;

                case "VI02.1":           //Vork Motor Transportation
                    link = "http://www.vorkmotor.com/tracking.php";
                    break;

                case "WA01.1":           //Ward Trucking
                    link = "http://www.wardtrucking.com/web/wardwebs.nsf/homeform?openform";
                    break;

                case "YE01.1":           //Yellow Freight  now YRC
                    link = "http://my.yrc.com/dynamic/national/servlet?CONTROLLER=com.rdwy.ec.rextracking.http.controller.ProcessPublicTrackingController&PRONumber=" + trackingNumber.Replace("-", "");
                    break;

                default:
                    link = "";
                    break;
            }

            return link;
        }
    }
}