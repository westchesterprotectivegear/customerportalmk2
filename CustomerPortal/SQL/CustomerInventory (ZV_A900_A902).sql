SELECT        (CASE ISNUMERIC(prd.KNVV.KUNNR) WHEN 1 THEN substring(prd.KNVV.KUNNR, patindex('%[^0]%', prd.KNVV.KUNNR), 10) ELSE prd.KNVV.KUNNR END) AS CustomerNumber, (CASE ISNUMERIC(prd.A902.MATNR)
                          WHEN 1 THEN substring(prd.A902.MATNR, patindex('%[^0]%', prd.A902.MATNR), 10) ELSE prd.A902.MATNR END) AS SKU, KONP_2.KBETR AS PriceListPrice, SpecificTable.KBETR AS CustomerSpecificPrice, 
                         REPLACE(KONP_2.KMEIN, 'PAA', 'PR') AS UnitOfMeasure, prd.MAKT.MAKTX AS Description, prd.A902.PLTYP AS PriceList, prd.A902.DATAB AS ValidStart, prd.A902.DATBI AS ValidEnd, 
                         ABS(DiscountTable.KBETR * .001) AS Discount
FROM            prd.A902 INNER JOIN
                         prd.KONP AS KONP_2 ON prd.A902.MANDT = KONP_2.MANDT AND prd.A902.KNUMH = KONP_2.KNUMH INNER JOIN
                         prd.MAKT ON prd.A902.MANDT = prd.MAKT.MANDT AND prd.A902.MATNR = prd.MAKT.MATNR INNER JOIN
                         prd.T006A ON prd.A902.MANDT = prd.T006A.MANDT AND KONP_2.KMEIN = prd.T006A.MSEHI AND prd.MAKT.SPRAS = prd.T006A.SPRAS INNER JOIN
                         prd.KNVV ON prd.A902.PLTYP = REPLACE(prd.KNVV.PLTYP, ' ', 'A') LEFT OUTER JOIN
                             (SELECT        prd.A906.MATNR, prd.KONP.KBETR
                               FROM            prd.A906 INNER JOIN
                                                         prd.KONP ON prd.A906.MANDT = prd.KONP.MANDT AND prd.A906.KNUMH = prd.KONP.KNUMH
                               WHERE        (prd.A906.MANDT = '100') AND (prd.A906.KAPPL = 'V') AND (prd.A906.KSCHL = 'ZPRM') AND (prd.A906.DATAB <= REPLACE(CONVERT(char(10), GETDATE(), 126), '-', '')) AND 
                                                         (prd.A906.DATBI > REPLACE(CONVERT(char(10), GETDATE(), 126), '-', '')) AND (prd.KONP.KONWA = '%')) AS DiscountTable ON prd.A902.MATNR = DiscountTable.MATNR LEFT OUTER JOIN
                             (SELECT        prd.A900.MATNR, prd.A900.KUNNR, KONP_1.KBETR
                               FROM            prd.A900 INNER JOIN
                                                         prd.KONP AS KONP_1 ON prd.A900.MANDT = KONP_1.MANDT AND prd.A900.KNUMH = KONP_1.KNUMH
                               WHERE        (prd.A900.MANDT = '100') AND (prd.A900.KAPPL = 'V') AND (prd.A900.KSCHL = 'ZPR0') AND (prd.A900.DATAB <= REPLACE(CONVERT(char(10), GETDATE(), 126), '-', '')) AND 
                                                         (prd.A900.DATBI > REPLACE(CONVERT(char(10), GETDATE(), 126), '-', ''))) AS SpecificTable ON prd.A902.MATNR = SpecificTable.MATNR AND prd.KNVV.KUNNR = SpecificTable.KUNNR
WHERE        (prd.A902.MANDT = '100') AND (prd.A902.DATBI > REPLACE(CONVERT(char(10), GETDATE(), 126), '-', '')) AND (prd.A902.DATAB <= REPLACE(CONVERT(char(10), GETDATE(), 126), '-', '')) AND (prd.MAKT.SPRAS = 'E') 
                         AND (prd.A902.KAPPL = 'V') AND (prd.A902.KSCHL = 'ZPR0') AND (prd.KNVV.VKORG = 0010) AND (prd.KNVV.SPART = 00) AND (prd.KNVV.VTWEG = 'IN' OR
                         prd.KNVV.VTWEG = 'CN')