﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SAP.Middleware.Connector;
using System.Data;
using CustomerPortal.Models;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using System.IO;
using System.Data.SqlClient;
using System.Threading;
using CustomerPortalModels;

namespace CustomerPortal
{
    public class OrderService
    {
        public CheckoutResponse PlaceOrder(CheckoutModel model, ShoppingCart cart)
        {
            CheckoutResponse response = new CheckoutResponse();

            var saleItems = cart.LineItems.Where(x => !x.Sample).ToList();
            var sampleItems = cart.LineItems.Where(x => x.Sample).ToList();

            if (saleItems.Count() > 0)
                response.OrderResponse = CreateOrder(model, saleItems, false);
            if (sampleItems.Count() > 0)
                response.SampleResponse = CreateSample(model, sampleItems, true);

            return response;
        }

        public OrderSubmissionResponse CreateOrder(CheckoutModel model, List<CartLineItem> lineItems, bool sampleOrder)
        {
            OrderSubmissionResponse response = new OrderSubmissionResponse();

            try
            {
                RfcDestination destination = RfcDestinationManager.GetDestination("mySAPdestination");
                RfcRepository repo = destination.Repository;
                IRfcFunction salesDoc = repo.CreateFunction("BAPI_SALESORDER_CREATEFROMDAT2");
                IRfcFunction salesDocCommit = repo.CreateFunction("BAPI_TRANSACTION_COMMIT");

                IRfcStructure salesHeader = salesDoc.GetStructure("ORDER_HEADER_IN");
                IRfcStructure salesHeaderX = salesDoc.GetStructure("ORDER_HEADER_INX");

                IRfcTable salesItems = salesDoc.GetTable("ORDER_ITEMS_IN");
                IRfcTable scheduleItems = salesDoc.GetTable("ORDER_SCHEDULES_IN");
                IRfcTable salesPartners = salesDoc.GetTable("ORDER_PARTNERS");
                IRfcTable returnTable = salesDoc.GetTable("RETURN");

                IRfcStructure shipToStruct = salesPartners.Metadata.LineType.CreateStructure();
                IRfcStructure soldToStruct = salesPartners.Metadata.LineType.CreateStructure();

                //Payment Terms
                string terms1 = model.PaymentTerms;
                string terms2 = model.AccountNumber;

                //Prepaid over $2000 or telesales over $500
                if ((terms1 == "ADD" && lineItems.Sum(x => x.TotalPrice) > 2000) || (model.Telesales && lineItems.Sum(x => x.TotalPrice) >= 700))
                    terms1 = "PPD";

                //Sales Header
                if (sampleOrder)
                    salesHeader.SetValue("DOC_TYPE", "ZSM2");
                else
                    salesHeader.SetValue("DOC_TYPE", "ZEDI");

                salesHeader.SetValue("SALES_ORG", "0010");
                salesHeader.SetValue("DISTR_CHAN", "IN");
                salesHeader.SetValue("DIVISION", "00");
                salesHeader.SetValue("PURCH_DATE", DateTime.Now.ToString("yyyyMMdd"));
                salesHeader.SetValue("INCOTERMS1", terms1);
                salesHeader.SetValue("INCOTERMS2", terms2);
                salesHeader.SetValue("CUST_GRP1", "IN2");
                salesHeader.SetValue("CURRENCY", "USD  ");
				if (lineItems[0].Product.CustomerNumber != "SS14")
                    salesHeader.SetValue("DLV_BLOCK", "04");

                salesHeader.SetValue("PURCH_NO_C", model.PONumber);
                salesHeader.SetValue("H_CURR", "USD  ");
                salesHeader.SetValue("PRICE_DATE", DateTime.Now.ToString("yyyyMMdd"));
                salesHeader.SetValue("REF_1", model.CustomerPONumber);
                salesHeader.SetValue("PURCH_NO_S", model.CustomerPONumber);
                salesHeaderX.SetValue("DOC_TYPE", "X");
                salesHeaderX.SetValue("SALES_ORG", "X");
                salesHeaderX.SetValue("DISTR_CHAN", "X");
                salesHeaderX.SetValue("DIVISION", "X");
                salesHeaderX.SetValue("PURCH_DATE", "X");
                salesHeaderX.SetValue("INCOTERMS1", "X");
                salesHeaderX.SetValue("INCOTERMS2", "X");
                salesHeaderX.SetValue("CUST_GRP1", "X");
                salesHeaderX.SetValue("PURCH_NO_C", "X");
                salesHeaderX.SetValue("CURRENCY", "X");
                salesHeaderX.SetValue("H_CURR", "X");
                salesHeaderX.SetValue("PRICE_DATE", "X");
                salesHeaderX.SetValue("REF_1", "X");
                salesHeaderX.SetValue("PURCH_NO_S", "X");
                salesHeaderX.SetValue("UPDATEFLAG", "I");
                salesHeaderX.SetValue("DLV_BLOCK", "X");

                //Sales Items
                for (int i = 0; i < lineItems.Count; i++)
                {
                    var lineItem = lineItems[i];
                    string lineNumber = ((i + 1) * 10).ToString("000000");

                    //Fix UOM
                    if (lineItem.Product.UnitOfMeasure == "PR")
                        lineItem.Product.UnitOfMeasure = "PAA";

                    IRfcStructure salesItemsStruct = salesItems.Metadata.LineType.CreateStructure();
                    IRfcStructure scheduleItemsStruct = scheduleItems.Metadata.LineType.CreateStructure();
                    salesItemsStruct.SetValue("ITM_NUMBER", lineNumber);
                    salesItemsStruct.SetValue("MATERIAL", lineItem.Product.SKU.PadSAP(18));
                    salesItemsStruct.SetValue("PLANT", "MONR");
                    salesItemsStruct.SetValue("TARGET_QTY", lineItem.Quantity);
                    salesItemsStruct.SetValue("TARGET_QU", lineItem.Product.UnitOfMeasure);
                    salesItemsStruct.SetValue("SHIP_POINT", "MONR");
                    salesItemsStruct.SetValue("INCOTERMS1", terms1);
                    salesItemsStruct.SetValue("INCOTERMS2", terms2);
                    salesItemsStruct.SetValue("ROUTE", "WCHUPS");
                    salesItems.Append(salesItemsStruct); // Append to Structure

                    scheduleItemsStruct.SetValue("ITM_NUMBER", lineNumber);
                    scheduleItemsStruct.SetValue("REQ_QTY", lineItem.Quantity);
                    scheduleItems.Append(scheduleItemsStruct); // Append to Structure
                }

                shipToStruct.SetValue("PARTN_ROLE", "WE"); //Ship to Party
                shipToStruct.SetValue("PARTN_NUMB", model.ShipToID == "1TIME" ? "PORTAL" : model.ShipToID);
                shipToStruct.SetValue("NAME", model.Name);
                shipToStruct.SetValue("NAME_2", model.Attention);
                shipToStruct.SetValue("NAME_3", "");
                shipToStruct.SetValue("NAME_4", "");
                shipToStruct.SetValue("STREET", model.Street);
                shipToStruct.SetValue("COUNTRY", "US");
                shipToStruct.SetValue("POSTL_CODE", model.Zip);
                shipToStruct.SetValue("CITY", model.City);
                shipToStruct.SetValue("REGION", model.State);
                shipToStruct.SetValue("LANGU", "EN");
                salesPartners.Append(shipToStruct); // Append to Structure

                // Partner
                soldToStruct.SetValue("PARTN_ROLE", "AG"); //Sold to Party 
                soldToStruct.SetValue("PARTN_NUMB", lineItems[0].Product.CustomerNumber);
                salesPartners.Append(soldToStruct); // Append to Structure


                RfcSessionManager.BeginContext(destination);
                salesDoc.Invoke(destination);
                salesDocCommit.Invoke(destination);
                var table = returnTable.ToDataTable("table");

                var status = table.Rows[table.Rows.Count - 1][0].ToString();
                if (status == "E")
                {
                    response.Status = false;
                    response.Message = "There was an error creating the order\n" + table.Rows[table.Rows.Count - 2][3].ToString()+" \n " + String.Join(" | ",table.Rows[table.Rows.Count - 1].ItemArray);
                    var emailbody = response.Message + " \n " + model.ShipToID + " \n ";
                    EmailService.SendMessage("Error Processing Order", emailbody);
                }
                else
                {
                    response.Message = "Your purchase order was placed successfully!";
                    response.Status = true;
                    response.OrderNumber = table.Rows[table.Rows.Count - 1][3].ToString().Substring(16, 7);

                    Thread.Sleep(5000);

                    //The inco terms do not work on the order creation for whatever reason. So we have to run an update here.
                    salesDoc = repo.CreateFunction("BAPI_SALESORDER_CHANGE");
                    salesDocCommit = repo.CreateFunction("BAPI_TRANSACTION_COMMIT");
                    salesDoc.SetValue("SALESDOCUMENT", response.OrderNumber.PadSAP(10));
                    salesDocCommit.SetValue("Wait", "X");

                    salesHeader = salesDoc.GetStructure("ORDER_HEADER_IN");
                    salesHeaderX = salesDoc.GetStructure("ORDER_HEADER_INX");
                    returnTable = salesDoc.GetTable("RETURN");

                    salesHeader.SetValue("INCOTERMS1", terms1);
                    salesHeader.SetValue("INCOTERMS2", terms2);
                    salesHeaderX.SetValue("INCOTERMS1", "X");
                    salesHeaderX.SetValue("INCOTERMS2", "X");
                    salesHeaderX.SetValue("UPDATEFLAG", "U");

                    RfcSessionManager.BeginContext(destination);
                    salesDoc.Invoke(destination);
                    salesDocCommit.Invoke(destination);
                    var table2 = returnTable.ToDataTable("table");
                    System.Diagnostics.Debug.Write(table2.Rows.Count);

                    RfcSessionManager.EndContext(destination);
                }
            }
            catch(Exception e)
            {
                response.Status = false;
                EmailService.SendMessage("Error Processing Order", e.Message + "\n" + e.InnerException + "\n" + e.StackTrace);
            }

            return response;
        }

        public OrderSubmissionResponse CreateSample(CheckoutModel model, List<CartLineItem> lineItems, bool sampleOrder)
        {
            OrderSubmissionResponse response = new OrderSubmissionResponse();

            RfcDestination destination = RfcDestinationManager.GetDestination("mySAPdestination");
            RfcRepository repo = destination.Repository;
            IRfcFunction salesDoc = repo.CreateFunction("BAPI_QUOTATION_CREATEFROMDATA");
            IRfcFunction salesDocCommit = repo.CreateFunction("BAPI_TRANSACTION_COMMIT");

            IRfcStructure salesHeader = salesDoc.GetStructure("ORDER_HEADER_IN");

            IRfcTable salesItems = salesDoc.GetTable("ORDER_ITEMS_IN");
            IRfcTable salesPartners = salesDoc.GetTable("ORDER_PARTNERS");
            IRfcStructure returnTable = salesDoc.GetStructure("RETURN");
            salesDoc.GetStructure("SALESDOCUMENT");

            IRfcStructure shipToStruct = salesPartners.Metadata.LineType.CreateStructure();
            IRfcStructure soldToStruct = salesPartners.Metadata.LineType.CreateStructure();

            //Payment Terms
            string terms1 = model.PaymentTerms;
            string terms2 = model.AccountNumber;

            //Sales Header
            if (sampleOrder)
                salesHeader.SetValue("DOC_TYPE", "ZSM2");
            else
                salesHeader.SetValue("DOC_TYPE", "ZEDI");

            salesHeader.SetValue("SALES_ORG", "0010");
            salesHeader.SetValue("DISTR_CHAN", "IN");
            salesHeader.SetValue("DIVISION", "00");
            salesHeader.SetValue("PURCH_DATE", DateTime.Now.ToString("yyyyMMdd"));
            salesHeader.SetValue("INCOTERMS1", model.PaymentTerms);
            salesHeader.SetValue("INCOTERMS2", model.AccountNumber);
            salesHeader.SetValue("CUST_GRP1", "IN2");
            salesHeader.SetValue("CURRENCY", "USD  ");
            salesHeader.SetValue("DLV_BLOCK", "SB");
            salesHeader.SetValue("PURCH_NO_C", model.PONumber);
            salesHeader.SetValue("CURRENCY", "USD  ");
            salesHeader.SetValue("PRICE_DATE", DateTime.Now.ToString("yyyyMMdd"));
            salesHeader.SetValue("REF_1", model.CustomerPONumber);
            salesHeader.SetValue("PURCH_NO_S", model.CompanyName);

            //Sales Items
            for (int i = 0; i < lineItems.Count; i++)
            {
                var lineItem = lineItems[i];
                string lineNumber = ((i + 1) * 10).ToString("000000");

                //Fix UOM
                if (lineItem.Product.UnitOfMeasure == "PR")
                    lineItem.Product.UnitOfMeasure = "PAA";

                IRfcStructure salesItemsStruct = salesItems.Metadata.LineType.CreateStructure();
                salesItemsStruct.SetValue("ITM_NUMBER", lineNumber);
                salesItemsStruct.SetValue("MATERIAL", lineItem.Product.SKU.PadSAP(18));
                salesItemsStruct.SetValue("PLANT", "MONR");
                salesItemsStruct.SetValue("REQ_QTY", 1000);

                // Samples all use the sample uom - "SM"
                salesItemsStruct.SetValue("TARGET_QU", "SM");
                salesItemsStruct.SetValue("SHIP_POINT", "MONR");
                salesItems.Append(salesItemsStruct); // Append to Structure
            }

            shipToStruct.SetValue("PARTN_ROLE", "WE"); //Ship to Party
            shipToStruct.SetValue("PARTN_NUMB", model.ShipToID);
            shipToStruct.SetValue("NAME", model.Name);
            shipToStruct.SetValue("NAME_2", model.Attention);
            shipToStruct.SetValue("NAME_3", "");
            shipToStruct.SetValue("NAME_4", "");
            shipToStruct.SetValue("STREET", model.Street);
            shipToStruct.SetValue("COUNTRY", "US");
            shipToStruct.SetValue("POSTL_CODE", model.Zip);
            shipToStruct.SetValue("CITY", model.City);
            shipToStruct.SetValue("REGION", model.State);
            shipToStruct.SetValue("LANGU", "EN");
            salesPartners.Append(shipToStruct); // Append to Structure

            // Partner
            soldToStruct.SetValue("PARTN_ROLE", "AG"); //Sold to Party 
            soldToStruct.SetValue("PARTN_NUMB", lineItems[0].Product.CustomerNumber);
            salesPartners.Append(soldToStruct); // Append to Structure


            RfcSessionManager.BeginContext(destination);
            salesDoc.Invoke(destination);
            salesDocCommit.Invoke(destination);
            
            var status = salesDoc.GetStructure("RETURN").GetValue(0);
            if (status.ToString() == "E")
            {
                response.Status = false;
                response.Message = "There was an error creating the sample request.";
            }
            else
            {
                response.Message = "Your sample request was placed successfully!";
                response.Status = true;
                response.OrderNumber = salesDoc.GetValue("SALESDOCUMENT").ToString();
            }

            RfcSessionManager.EndContext(destination);
            return response;
        }

        public string PrintOrder(string orderNumber)
        {
            RfcDestination destination = RfcDestinationManager.GetDestination("mySAPdestination");
            RfcRepository repo = destination.Repository;
            IRfcFunction salesDoc = repo.CreateFunction("ZRFC_REPRINT_TO_PDF");
            salesDoc.SetValue("APPLICATION", "V1");
            salesDoc.SetValue("DOCTYPE", "BA00");
            salesDoc.SetValue("DOCNUMBER", orderNumber.PadSAP(10));
            salesDoc.SetValue("DIRECTORY", @"\\FRED\REPRINT");

            RfcSessionManager.BeginContext(destination);
            salesDoc.Invoke(destination);
            RfcSessionManager.EndContext(destination);
            return orderNumber.PadSAP(10) + ".pdf";
        }

        public string PrintPackingSlip(string orderNumber)
        {
            RfcDestination destination = RfcDestinationManager.GetDestination("mySAPdestination");
            RfcRepository repo = destination.Repository;
            IRfcFunction salesDoc = repo.CreateFunction("ZRFC_REPRINT_TO_PDF");
            salesDoc.SetValue("APPLICATION", "V2");
            salesDoc.SetValue("DOCNUMBER", orderNumber.PadSAP(10));
            salesDoc.SetValue("DIRECTORY", @"\\FRED\REPRINT");

            var query = @"SELECT     KSCHL FROM    prd.NAST WHERE     (MANDT = '100') AND (OBJKY = (:orderNumber)) AND (KAPPL = 'V2') AND (SPRAS = 'E') AND (KSCHL LIKE 'ZPK%')";

            var session = SAPSessionFactory.GetCurrentSession();
            var result = session.CreateSQLQuery(query)
                            .SetString("orderNumber", orderNumber.PadSAP(10))
                            .UniqueResult();

            if (result == null)
                salesDoc.SetValue("DOCTYPE", "ZPKK");
            else
                salesDoc.SetValue("DOCTYPE", result.ToString());

            RfcSessionManager.BeginContext(destination);
            salesDoc.Invoke(destination);
            RfcSessionManager.EndContext(destination);
            return orderNumber.PadSAP(10) + ".pdf";
        }
    }

    public static class SAPExtensions
    {
        public static string PadSAP(this String str, int length)
        {
            if (Regex.IsMatch(str, @"^\d+$"))
                return str.PadLeft(length, '0');
            else
                return str;
        }

        public static string StripSAP(this String str)
        {
            if (Regex.IsMatch(str, @"^\d+$"))
                return str.TrimStart('0');
            else
                return str;
        }
    }
}