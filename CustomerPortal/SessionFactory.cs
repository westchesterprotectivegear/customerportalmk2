﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NHibernate;
using FluentNHibernate.Cfg;
using NHibernate.Context;
using FluentNHibernate.Cfg.Db;
using FluentNHibernate.Automapping;
using FluentNHibernate.Conventions.Helpers;
using System.Reflection;
using NHibernate.Cfg;
using System.Diagnostics;
using System.Configuration;
using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.Instances;
using NHibernate.Caches.SysCache;

namespace CustomerPortal
{
    public class CustomerPortalSessionFactory : IHttpModule
    {
        private static readonly ISessionFactory _SessionFactory;

        static CustomerPortalSessionFactory()
        {
            _SessionFactory = CreateSessionFactory();
        }

        public void Init(HttpApplication context)
        {
            context.BeginRequest += BeginRequest;
            context.EndRequest += EndRequest;
        }

        public void Dispose()
        {
        }

        public static ISession GetCurrentSession()
        {
            if (!CurrentSessionContext.HasBind(_SessionFactory))
                CurrentSessionContext.Bind(_SessionFactory.OpenSession());

            return _SessionFactory.GetCurrentSession();
        }

        public static void DisposeCurrentSession()
        {
            ISession currentSession = CurrentSessionContext.Unbind(_SessionFactory);

            currentSession.Close();
            currentSession.Dispose();
        }

        private static void BeginRequest(object sender, EventArgs e)
        {
            ISession session = _SessionFactory.OpenSession();
            session.BeginTransaction();
            CurrentSessionContext.Bind(session);
        }

        private static void EndRequest(object sender, EventArgs e)
        {
            ISession session = CurrentSessionContext.Unbind(_SessionFactory);
            if (session == null) return;
            try
            {
                session.Transaction.Commit();
            }
            catch (Exception)
            {
                session.Transaction.Rollback();
            }
            finally
            {
                session.Close();
                session.Dispose();
            }
        }

        public static ISessionFactory CreateSessionFactory()
        {
            return Fluently.Configure()
                           .Database(MsSqlConfiguration.MsSql2008.ConnectionString(c => c.FromConnectionStringWithKey("CustomerPortalConnection")))
                           .Cache(c => c.ProviderClass<SysCacheProvider>().UseQueryCache().UseSecondLevelCache())
                           .Mappings(m => m.FluentMappings.AddFromAssembly(Assembly.Load("CustomerPortalModels")))
                           .CurrentSessionContext<WebSessionContext>()
                           .BuildSessionFactory();
        }
    }
    public class PMSessionFactory : IHttpModule
    {
        private static readonly ISessionFactory _SessionFactory;

        static PMSessionFactory()
        {
            _SessionFactory = CreateSessionFactory();
        }

        public void Init(HttpApplication context)
        {
            context.BeginRequest += BeginRequest;
            context.EndRequest += EndRequest;
        }

        public void Dispose()
        {
        }

        public static ISession GetCurrentSession()
        {
            if (!CurrentSessionContext.HasBind(_SessionFactory))
                CurrentSessionContext.Bind(_SessionFactory.OpenSession());

            return _SessionFactory.GetCurrentSession();
        }

        public static void DisposeCurrentSession()
        {
            ISession currentSession = CurrentSessionContext.Unbind(_SessionFactory);

            currentSession.Close();
            currentSession.Dispose();
        }

        private static void BeginRequest(object sender, EventArgs e)
        {
            ISession session = _SessionFactory.OpenSession();
            session.BeginTransaction();
            CurrentSessionContext.Bind(session);
        }

        private static void EndRequest(object sender, EventArgs e)
        {
            ISession session = CurrentSessionContext.Unbind(_SessionFactory);
            if (session == null) return;
            try
            {
                session.Transaction.Commit();
            }
            catch (Exception)
            {
                session.Transaction.Rollback();
            }
            finally
            {
                session.Close();
                session.Dispose();
            }
        }

        private static ISessionFactory CreateSessionFactory()
        {
            return Fluently.Configure()
                           .Database(MsSqlConfiguration.MsSql2008.ConnectionString(c => c.FromConnectionStringWithKey("ProductManagerConnection")))
                           .Mappings(m => m.FluentMappings.AddFromAssembly(Assembly.Load("ProductManagerModels")))
                           .CurrentSessionContext<WebSessionContext>()
                           .BuildSessionFactory();
        }
    }

    public class SAPSessionFactory : IHttpModule
    {
        private static readonly ISessionFactory _SessionFactory;

        static SAPSessionFactory()
        {
            _SessionFactory = CreateSessionFactory();
        }

        public void Init(HttpApplication context)
        {
            context.BeginRequest += BeginRequest;
            context.EndRequest += EndRequest;
        }

        public void Dispose()
        {
        }

        public static ISession GetCurrentSession()
        {
            if (!CurrentSessionContext.HasBind(_SessionFactory))
                CurrentSessionContext.Bind(_SessionFactory.OpenSession());

            return _SessionFactory.GetCurrentSession();
        }

        public static void DisposeCurrentSession()
        {
            ISession currentSession = CurrentSessionContext.Unbind(_SessionFactory);

            currentSession.Close();
            currentSession.Dispose();
        }

        private static void BeginRequest(object sender, EventArgs e)
        {
            ISession session = _SessionFactory.OpenSession();
            session.BeginTransaction();
            CurrentSessionContext.Bind(session);
        }

        private static void EndRequest(object sender, EventArgs e)
        {
            ISession session = CurrentSessionContext.Unbind(_SessionFactory);
            if (session == null) return;
            try
            {
                session.Transaction.Commit();
            }
            catch (Exception)
            {
                session.Transaction.Rollback();
            }
            finally
            {
                session.Close();
                session.Dispose();
            }
        }

        private static ISessionFactory CreateSessionFactory()
        {
            return Fluently.Configure()
                           .Database(MsSqlConfiguration.MsSql2000.ConnectionString(c => c.FromConnectionStringWithKey("SAPConnection")))
                           .Mappings(m => m.FluentMappings.AddFromAssembly(Assembly.Load("SAPModels")).Conventions.Add<SAPStringPropertyConvention>())
                           //.ExposeConfiguration(x => x.SetInterceptor(new SqlStatementInterceptor()))
                           .CurrentSessionContext<WebSessionContext>()
                           .BuildSessionFactory();
        }
    }

    public class SAPStringPropertyConvention : IPropertyConvention
    {
        public void Apply(IPropertyInstance instance)
        {
            if (instance.Property.PropertyType == typeof(string))
                instance.CustomType("AnsiString");
        }
    }
}