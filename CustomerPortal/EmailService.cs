﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;

namespace CustomerPortal
{
    public static class EmailService
    {
        private static SmtpClient smtp = new SmtpClient("192.168.100.18", 25);
        //private static SmtpClient smtp = new SmtpClient("192.168.100.125", 25);
        private static string defaultRecipient = "mmoore@westchestergear.com";

        public static void SendMessage(string subject, string message)
        {
            SendMessage(subject, message, defaultRecipient);
        }

        public static void SendMessage(string subject, string message, string recipient)
        {
            try
            {
                MailMessage email = new MailMessage();
                email.To.Add(recipient);
                email.Subject = subject;
                email.From = new MailAddress("CustomerPortal@westchestergear.com");
                email.Body = message;
                smtp.Send(email);
            }
            catch (Exception e)
            {
                string test = e.Message;
            }
        }

        public static void SendMessage(string subject, string message, List<string> recipients)
        {
            try
            {
                MailMessage email = new MailMessage();
                foreach (var recipient in recipients)
                    email.To.Add(recipient);
                email.Subject = subject;
                email.From = new MailAddress("CustomerPortal@westchestergear.com");
                email.Body = message;
                smtp.Send(email);
            }
            catch { }
        }

        public static void SendMessage(string subject, string message, List<string> recipients, List<string> attachments)
        {
            try
            {
                MailMessage email = new MailMessage();
                foreach (var recipient in recipients)
                    email.To.Add(recipient);
                foreach (var att in attachments)
                    email.Attachments.Add(new Attachment(att));
                email.Subject = subject;
                email.From = new MailAddress("CustomerPortal@westchestergear.com");
                email.Body = message;
                smtp.Send(email);
            }
            catch { }
        }
    }

    public enum EmailHost
    {
        Fred,
        Filesrv
    }
}
