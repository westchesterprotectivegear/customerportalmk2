﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SAP.Middleware.Connector;
using System.Data;
using CustomerPortal.Models;
using System.Text.RegularExpressions;

namespace CustomerPortal
{
    public class InvoiceService
    {
        public string PrintInvoice(string invoiceNumber)
        {
            try
            {
                RfcDestination destination = RfcDestinationManager.GetDestination("mySAPdestination");
                RfcRepository repo = destination.Repository;
                IRfcFunction salesDoc = repo.CreateFunction("ZRFC_REPRINT_TO_PDF");
                salesDoc.SetValue("APPLICATION", "V3");
                salesDoc.SetValue("DOCTYPE", "RD00");
                salesDoc.SetValue("DOCNUMBER", invoiceNumber.PadSAP(10));
                salesDoc.SetValue("DIRECTORY", @"\\FRED\REPRINT");

                RfcSessionManager.BeginContext(destination);
                salesDoc.Invoke(destination);
                RfcSessionManager.EndContext(destination);
            }
            catch
            {
                return invoiceNumber.PadSAP(10) + ".pdf";
            }

            return invoiceNumber.PadSAP(10) + ".pdf";
        }
    }
}