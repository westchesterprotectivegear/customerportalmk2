﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CustomerPortalModels;

namespace CustomerPortal.Filters
{
    public class CustomerNumberRequiredAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var user = (User)filterContext.HttpContext.Session["user"];
          
            if (string.IsNullOrEmpty(user.CustomerNumber))
            {
                filterContext.Result = new RedirectResult("~/Home/CustomerNumberRequired");
            }
        }
    }
}