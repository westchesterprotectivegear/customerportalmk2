﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;
using System.IO;
using System.Runtime.Serialization;

namespace CustomerPortal
{
    public static class SerializationService
    {
        public static string SerializeObject<T>(this T toSerialize)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(toSerialize.GetType());
            StringWriter textWriter = new StringWriter();

            xmlSerializer.Serialize(textWriter, toSerialize);
            return textWriter.ToString();
        }

        public static object DeserializeObject<T>(string toDeserialize)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
            StringReader textReader = new StringReader(toDeserialize);

            return xmlSerializer.Deserialize(textReader);
        }

        public static string DataContractSerializeObject<T>(T objectToSerialize)
        {
            using (MemoryStream memStm = new MemoryStream())
            {
                try
                {
                    var serializer = new DataContractSerializer(typeof(T));
                    serializer.WriteObject(memStm, objectToSerialize);
                }
                catch { }

                memStm.Seek(0, SeekOrigin.Begin);

                using (var streamReader = new StreamReader(memStm))
                {
                    string result = streamReader.ReadToEnd();
                    return result;
                }
            }
        }

        public static object DataContractDeserializeObject<T>(T objectToSerialize, string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;

            try
            {
                var serializer = new DataContractSerializer(typeof(T));
                return serializer.ReadObject(stream);
            }
            catch
            {
                return null;
            }
        }
    }
}