﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CustomerPortal.Models;
using System.IO;
using ProductManagerModels;
using System.Drawing;
using CustomerPortalModels;

namespace CustomerPortal.Controllers
{
    [Authorize]
    public class ResourceController : _ApplicationController
    {
        private readonly ICrossReferenceRepository _xRefRepo;
        private readonly IUserActivityRepository _userActivityRepo;

        public ResourceController(ICrossReferenceRepository xrefRepo, IUserActivityRepository userActivityRepo)
        {
            _xRefRepo = xrefRepo;
            _userActivityRepo = userActivityRepo;
        }        
        
        public ActionResult Index(string path)
        {
            string realPath;
            realPath = @"\\fred\reprint\Documents\Downloads\" + path;
            // or realPath = "FullPath of the folder on server" 

            if (System.IO.File.Exists(realPath))
            {
                //http://stackoverflow.com/questions/1176022/unknown-file-type-mime
                return base.File(realPath, "application/octet-stream");
            }
            else if (System.IO.Directory.Exists(realPath))
            {

                Uri url = Request.Url;
                //Every path needs to end with slash
                if (url.ToString().Last() != '/')
                {
                    Response.Redirect("/Resource/" + path + "/");
                }

                List<FileModel> fileListModel = new List<FileModel>();

                List<DirectoryModel> dirListModel = new List<DirectoryModel>();

                IEnumerable<string> dirList = Directory.EnumerateDirectories(realPath);
                foreach (string dir in dirList)
                {
                    DirectoryInfo d = new DirectoryInfo(dir);

                    DirectoryModel dirModel = new DirectoryModel();

                    dirModel.DirName = Path.GetFileName(dir);
                    dirModel.DirAccessed = d.LastAccessTime;

                    dirListModel.Add(dirModel);
                }

                IEnumerable<string> fileList = Directory.EnumerateFiles(realPath);
                foreach (string file in fileList)
                {
                    FileInfo f = new FileInfo(file);

                    FileModel fileModel = new FileModel();

                    if (f.Extension.ToLower() != "php" && f.Extension.ToLower() != "aspx"
                        && f.Extension.ToLower() != "asp")
                    {
                        fileModel.FileName = Path.GetFileName(file);
                        fileModel.FileAccessed = f.LastAccessTime;
                        fileModel.FileSizeText = (f.Length < 1024) ? f.Length.ToString() + " B" : f.Length / 1024 + " KB";

                        fileListModel.Add(fileModel);
                    }
                }

                FileExplorerModel explorerModel = new FileExplorerModel(dirListModel, fileListModel);

                return View(explorerModel);
            }
            else
            {
                return Content(path + " is not a valid file or directory.");
            }
        }

        public ActionResult CrossReferenceTool()
        {
            var model = new CrossReferenceToolModel()
            {
                Companies = _xRefRepo.GetCompanies(),
                CrossReferences = new List<CrossReference>()
            };
            return View(model);
        }

        public ActionResult _CrossReferenceTilePartial()
        {
            var model = new CrossReferenceToolModel()
            {
                Companies = _xRefRepo.GetCompanies().OrderBy(x => x).ToList(),
                CrossReferences = new List<CrossReference>()
            };
            return PartialView(model);
        }

        [HttpPost]
        public ActionResult CrossReferenceSearch(CrossReferenceSearchModel search)
        {
            var model = new CrossReferenceToolModel()
            {
                Companies = _xRefRepo.GetCompanies(),
                CrossReferences = _xRefRepo.GetAll(search)
            };
            return View("CrossReferenceTool", model);
        }

        public ActionResult MobileApps()
        {
            return View();
        }

        public ActionResult Downloads()
        {
            return View(new DirectoryInfo(@"\\fred\Reprint\Documents\Downloads"));
        }

        public ActionResult FolderLink(DirectoryInfo directory)
        {
            ViewData["fileCount"] = GetFileCount(directory);
            return PartialView(directory);
        }

        public int GetFileCount(DirectoryInfo directory)
        {
            //Gets non-hidden files
            int count = directory.GetFiles("*", SearchOption.AllDirectories).Count(x =>
                ((System.IO.File.GetAttributes(x.FullName)) & FileAttributes.Hidden) != FileAttributes.Hidden);

            return count;
        }

        [HttpPost]
        public ActionResult FileList(string info)
        {
            DirectoryInfo di = new DirectoryInfo(info);
            return PartialView(di);
        }

        public ActionResult FolderList(DirectoryInfo directory)
        {
            return PartialView(directory);
        }

        [HttpPost]
        public ActionResult FileSearchResults(string searchString)
        {
            DirectoryInfo di = new DirectoryInfo(@"\\fred\Reprint\Documents\Downloads");
            List<FileInfo> files = di.GetFiles("*" + searchString + "*", SearchOption.AllDirectories).ToList();
            return PartialView(files);
        }


        public ActionResult DownloadLink(string info)
        {
            Icon icon = IconService.GetFileIcon(info, IconService.IconSize.Large, false);
            if (icon == null)
                icon = new Icon(Server.MapPath("~/Images/DefaultIcon.ico"));
            byte[] imageData = IconToBytes(icon);
            return File(imageData, "image/jpeg");
        }

        public static byte[] IconToBytes(Icon icon)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                icon.Save(ms);
                return ms.ToArray();
            }
        }

        [HttpPost]
        public ActionResult BreadCrumb(string info)
        {
            DirectoryInfo di = new DirectoryInfo(info);
            return PartialView("BreadCrumb", di);
        }

        public void ServeFile(string filePath)
        {
            _userActivityRepo.Add(new UserActivity()
            {
                User = User.Identity.Name,
                Action = "File Download",
                Message = "Downloaded file: " + filePath.Remove(0, @"\\fred\Reprint\Documents\Downloads\".Length)
            });

            System.IO.Stream iStream = null;

            // Buffer to read 10K bytes in chunk:
            byte[] buffer = new Byte[10000];

            // Length of the file:
            int length;

            // Total bytes to read:
            long dataToRead;

            // Identify the file to download including its path.
            string filepath = filePath;

            // Identify the file name.
            string filename = System.IO.Path.GetFileName(filepath);

            try
            {
                // Open the file.
                iStream = new System.IO.FileStream(filepath, System.IO.FileMode.Open,
                            System.IO.FileAccess.Read, System.IO.FileShare.Read);


                // Total bytes to read:
                dataToRead = iStream.Length;

                Response.ContentType = MimeMapping.GetMimeMapping(filepath);
                Response.AddHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"");
                Response.AddHeader("Content-Length", iStream.Length.ToString());

                // Read the bytes.
                while (dataToRead > 0)
                {
                    // Verify that the client is connected.
                    if (Response.IsClientConnected)
                    {
                        // Read the data in buffer.
                        length = iStream.Read(buffer, 0, 10000);

                        // Write the data to the current output stream.
                        Response.OutputStream.Write(buffer, 0, length);

                        // Flush the data to the HTML output.
                        Response.Flush();

                        buffer = new Byte[10000];
                        dataToRead = dataToRead - length;
                    }
                    else
                    {
                        //prevent infinite loop if user disconnects
                        dataToRead = -1;
                    }
                }
            }
            catch (Exception ex)
            {
                // Trap the error, if any.
                Response.Write("Error : " + ex.Message);
            }
            finally
            {
                if (iStream != null)
                {
                    //Close the file.
                    iStream.Close();
                }
                Response.Close();
            }
        }
    }
}
