﻿using System.Linq;
using System.Web.Mvc;

namespace CustomerPortal.Controllers
{
    [Authorize]
    public class HelpController : _ApplicationController
    {
        private readonly IShipToRepository _shipToRepo;

        public HelpController(IShipToRepository shipToRepo)
        {
            _shipToRepo = shipToRepo;
        }

        public ActionResult Index()
        {
            return View();
        }
    }
}
