﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CustomerPortalModels;
using System.Web.Security;
using CustomerPortal.Models;
using System.Data.SqlClient;
using System.Configuration;


namespace CustomerPortal.Controllers
{
    [Authorize]
    public class ShipToController : _ApplicationController
    {
        private readonly IShipToRepository _shipToRepo;

        public ShipToController(IShipToRepository shipToRepo)
        {
            _shipToRepo = shipToRepo;
        }
        
        public ActionResult ShipToList()
        {
            //var shipTos = _shipToRepo.GetAll(GetUser().CustomerNumber).ToList();
			var custNumber = GetUser().CustomerNumber;
            string q = "";
            var shipTos = _shipToRepo.GetGroup(x => x.ID.StartsWith(custNumber) || x.ID.StartsWith(custNumber.StripSAP())).ToList();
            List<SAPModels.ShipTo> validshipto = new List<SAPModels.ShipTo>();
            validshipto = shipTos;
          /*  foreach (var st in shipTos)
            {
                q = "Select * from prd.KNVP where KUNN2='" + st.ID + "' and MANDT='100' and ";
                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SAPConnection"].ConnectionString);
                conn.Open();
                SqlCommand cmd = new SqlCommand(q, conn);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    validshipto.Add(st);
                }
                conn.Close();
            } */
            return PartialView(validshipto);
        }

        [HttpPost]
        public ActionResult ShipToList(ShipToSearchModel model)
        {
            //var shipTos = _shipToRepo.GetAll(GetUser().CustomerNumber);
			var custNumber = GetUser().CustomerNumber;
            var shipTos = _shipToRepo.GetGroup(x => x.ID.StartsWith(custNumber) || x.ID.StartsWith(custNumber.StripSAP())).ToList();
            if (!string.IsNullOrEmpty(model.State))
                shipTos = shipTos.Where(x => x.State.ToLower() == model.State.ToLower()).ToList();

            if (!string.IsNullOrEmpty(model.City))
                shipTos = shipTos.Where(x => x.City.ToLower().Contains(model.City.ToLower())).ToList();

            if (!string.IsNullOrEmpty(model.Street))
                shipTos = shipTos.Where(x => x.Street.ToLower().Contains(model.Street.ToLower())).ToList();

            if (!string.IsNullOrEmpty(model.Zip))
                shipTos = shipTos.Where(x => x.Zip.ToLower().Contains(model.Zip.ToLower())).ToList();

            if (shipTos.Count() == 0)
            {
                ViewData["emptyMessage"] = "Sorry! No results match your search terms.";
            }

            return PartialView(shipTos);
        }
    }
}
