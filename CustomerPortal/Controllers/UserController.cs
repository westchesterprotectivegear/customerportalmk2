﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CustomerPortalModels;
using System.Web.Security;
using CustomerPortal.Models;

namespace CustomerPortal.Controllers
{
    [Authorize]
    public class UserController : _ApplicationController
    {
        public UserController()
        {
        }

        public ActionResult Index()
        {
            var users = userRepo.GetAll().Take(100).ToList();
            return View(users);
        }

        [HttpPost]
        public ActionResult Index(string searchData)
        {
            searchData = searchData.ToLower();
            var users = userRepo.GetAll().Where(x => x.UserName.ToLower().Contains(searchData) || x.DisplayName.ToLower().Contains(searchData)).Take(100).ToList();
            return View(users);
        }

        [HttpPost]
        public ActionResult AdvancedSearch(UserAdvancedSearchModel model)
        {
            var userQuery = userRepo.GetAll();

            if (!string.IsNullOrEmpty(model.UserName))
                userQuery = userQuery.Where(x => x.UserName.ToLower().Contains(model.UserName.ToLower()));

            if (!string.IsNullOrEmpty(model.DisplayName))
                userQuery = userQuery.Where(x => x.DisplayName.ToLower().Contains(model.DisplayName.ToLower()));

            var users = userQuery.ToList();

            if (!string.IsNullOrEmpty(model.AccountType))
                users = users.Where(x => x.AccountType.ToLower().Contains(model.AccountType.ToLower())).ToList();

            if (model.AccountType == "Customer" && !string.IsNullOrEmpty(model.CustomerNumber))
                users = users.Where(x => x.CustomerNumber.ToLower() == model.CustomerNumber.ToLower()).ToList();

            if (users.Count() == 0)
            {
                ViewData["emptyMessage"] = "Sorry! No results match your search terms.";
            }

            return View("Index", users);
        }
    }
}
