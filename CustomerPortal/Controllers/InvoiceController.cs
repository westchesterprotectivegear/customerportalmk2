﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CustomerPortalModels;
using CustomerPortal.Models;
using NCommon.Specifications;
using SAPModels;

namespace CustomerPortal.Controllers
{
    [Authorize]
    public class InvoiceController : _ApplicationController
    {
        private readonly IInvoiceRepository _invoiceRepo;
        private readonly IUserActivityRepository _userActivityRepo;

        public InvoiceController(IInvoiceRepository invoiceRepo, IUserActivityRepository userActivityRepo)
        {
            _invoiceRepo = invoiceRepo;
            _userActivityRepo = userActivityRepo;
        }

        public ActionResult Index()
        {
            var invoices = _invoiceRepo.GetAll(GetUser().CustomerNumber).Where(x => x.InvoiceDate > DateTime.Now.AddDays(-30)).Take(100).ToList();
            //var invoices = _invoiceRepo.GetAll(new Specification<Invoice>(x => x.CustomerNumber == GetUser().CustomerNumber));
            return View(invoices);
        }

        [HttpPost]
        public ActionResult Index(string searchData)
        {
            searchData = searchData.ToLower();

            _userActivityRepo.Add(new UserActivity()
            {
                User = User.Identity.Name,
                Action = "Invoice Search",
                Message = "Searched for invoice: " + searchData
            });
            
            var invoices = _invoiceRepo.GetAll(new Specification<Invoice>(x => x.CustomerNumber == GetUser().CustomerNumber && (x.PONumber.ToLower().Contains(searchData) || x.InvoiceNumber.ToString().Contains(searchData))));
            return View(invoices);
        }

        [HttpPost]
        public ActionResult AdvancedSearch(InvoiceAdvancedSearchModel model)
        {
            var invoices = _invoiceRepo.GetAll(GetUser().CustomerNumber);

            if (!string.IsNullOrEmpty(model.StartDate))
                invoices = invoices.Where(x => x.InvoiceDate >= DateTime.Parse(model.StartDate));

            if (!string.IsNullOrEmpty(model.EndDate))
                invoices = invoices.Where(x => x.InvoiceDate <= DateTime.Parse(model.EndDate));

            if (!string.IsNullOrEmpty(model.InvoiceNumber))
                invoices = invoices.Where(x => x.InvoiceNumber.ToString().Contains(model.InvoiceNumber.ToLower()));

            if (!string.IsNullOrEmpty(model.PONumber))
                invoices = invoices.Where(x => x.PONumber.ToLower().Contains(model.PONumber.ToLower()));

            if (!string.IsNullOrEmpty(model.Status))
                invoices = invoices.Where(x => x.Status.ToLower().Contains(model.Status.ToLower()));

            if (model.AmountLow != 0.0)
                invoices = invoices.Where(x => x.InvoiceAmount >= model.AmountLow);

            if (model.AmountHigh != 0.0)
                invoices = invoices.Where(x => x.InvoiceAmount <= model.AmountHigh);

            var invoiceList = invoices.OrderByDescending(x => x.InvoiceDateSAP).Take(100).ToList();

            if (invoiceList.Count == 0)
            {
                ViewData["emptyMessage"] = "Sorry! No results match your search terms.";
            }

            return View("Index", invoiceList);
        }

        public ActionResult PrintInvoice(int invoiceNumber)
        {
            _userActivityRepo.Add(new UserActivity()
            {
                User = User.Identity.Name,
                Action = "Invoice Print",
                Message = "Printed invoice: " + invoiceNumber
            });

            string pdf = new InvoiceService().PrintInvoice(invoiceNumber.ToString());
            return File(@"\\fred\reprint\" + pdf, "application/pdf");
        }

        public ActionResult InvoiceTile()
        {
            try
            {
                var invoices = _invoiceRepo.GetAll(GetUser().CustomerNumber).Where(x => x.InvoiceDate > DateTime.Now.AddDays(-30)).Take(5).ToList();
                return PartialView(invoices);
            }
            catch (Exception)
            {
                return PartialView();
            }
        }
    }
}
