﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CustomerPortalModels;
using CustomerPortal.Filters;
using System.Data;
using System.Data.SqlClient;
using CustomerPortal.Models;
using NCommon.Specifications;
using SAPModels;

namespace CustomerPortal.Controllers
{
    [Authorize]
    [CustomerNumberRequired]
    public class OrderController : _ApplicationController
    {
        private readonly IOrderRepository _orderRepo;
        private readonly IOrderLineRepository _orderLineRepo;
        private readonly IUserActivityRepository _userActivityRepo;

        public OrderController(IOrderRepository orderRepo, IOrderLineRepository orderLineRepo, IUserActivityRepository userActivityRepo)
        {
            _orderRepo = orderRepo;
            _orderLineRepo = orderLineRepo;
            _userActivityRepo = userActivityRepo;
        }

        public ActionResult Index()
        {
            //var orders = _orderRepo.GetAll(GetUser().CustomerNumber).Take(100).ToList();
            var orders = _orderRepo.GetAll(new Specification<Order>(x => 
                x.SoldTo == GetUser().CustomerNumber));
            return View(orders);
        }

        [HttpPost]
        public ActionResult Index(string searchData)
        {
            searchData = searchData.ToLower();

            _userActivityRepo.Add(new UserActivity()
            {
                User = User.Identity.Name,
                Action = "Order Search",
                Message = "Searched for order: " + searchData
            });
            var orders = _orderRepo.GetAll(new Specification<Order>(x => 
                x.SoldTo == GetUser().CustomerNumber &&
                (x.SalesOrder.ToLower().Contains(searchData) || x.PurchaseOrder.ToLower().Contains(searchData))));
            return View(orders);
        }

        [HttpPost]
        public ActionResult AdvancedSearch(OrderAdvancedSearchModel model)
        {
            var orders = _orderRepo.GetAll(GetUser().CustomerNumber);

            var spec = new Specification<Order>(x =>
                !string.IsNullOrEmpty(model.StartDate) ? x.OrderDate >= DateTime.Parse(model.StartDate) : false &&
                !string.IsNullOrEmpty(model.EndDate) ? x.OrderDate <= DateTime.Parse(model.EndDate) : false &&
                !string.IsNullOrEmpty(model.OrderNumber) ? x.SalesOrder.ToLower().Contains(model.OrderNumber.ToLower()) : false
            );

            if (!string.IsNullOrEmpty(model.StartDate))
                orders = orders.Where(x => x.OrderDate >= DateTime.Parse(model.StartDate));

            if (!string.IsNullOrEmpty(model.EndDate))
                orders = orders.Where(x => x.OrderDate <= DateTime.Parse(model.EndDate));

            if (!string.IsNullOrEmpty(model.OrderNumber))
                orders = orders.Where(x => x.SalesOrder.ToLower().Contains(model.OrderNumber.ToLower()));

            if (!string.IsNullOrEmpty(model.PONumber))
                orders = orders.Where(x => x.PurchaseOrder.Contains(model.PONumber.ToLower()));

            if (!string.IsNullOrEmpty(model.Status))
                orders = orders.Where(x => x.Status.ToLower().Contains(model.Status.ToLower()));

            if (model.ValueLow != 0.0)
                orders = orders.Where(x => x.OrderValue >= model.ValueLow);

            if (model.ValueHigh != 0.0)
                orders = orders.Where(x => x.OrderValue <= model.ValueHigh);

            var orderList = orders.OrderByDescending(x => x.OrderDateSAP).Take(100).ToList();

            if (orderList.Count == 0)
            {
                ViewData["emptyMessage"] = "Sorry! No results match your search terms.";
            }

            return View("Index", orderList);
        }

        public ActionResult RecentOrderTile()
        {
            try
            {
                var orders = _orderRepo.GetAll(GetUser().CustomerNumber).Take(5).ToList();
                return PartialView(orders);
            }
            catch (Exception)
            {
                return PartialView();
            }
        }

        public ActionResult OrderDetail(string orderNumber)
        {
            var order = _orderRepo.Get(orderNumber, GetUser().CustomerNumber);
            order.LineItems = _orderLineRepo.Get(orderNumber).ToList();

            var sql = @"SELECT       PRD.prd.VBFA.VBELN, PRD.prd.LIKP.VBELN AS Delivery, PRD.prd.LIKP.ERDAT, PRD.prd.LIKP.BOLNR, PRD.prd.LIKP.ROUTE, PRD.prd.VTTK.TDLNR, PRD.prd.LFA1.NAME1, PRD.prd.TVROT.BEZEI
                      FROM         PRD.prd.VBFA INNER JOIN
                                   PRD.prd.LIKP ON PRD.prd.VBFA.MANDT = PRD.prd.LIKP.MANDT AND PRD.prd.VBFA.VBELN = PRD.prd.LIKP.VBELN INNER JOIN
                                   PRD.prd.VTTK ON PRD.prd.VBFA.MANDT = PRD.prd.VTTK.MANDT INNER JOIN
                                   PRD.prd.VTTP ON PRD.prd.VBFA.MANDT = PRD.prd.VTTP.MANDT AND PRD.prd.VBFA.VBELN = PRD.prd.VTTP.VBELN AND PRD.prd.VTTK.TKNUM = PRD.prd.VTTP.TKNUM INNER JOIN
                                   PRD.prd.LFA1 ON PRD.prd.VBFA.MANDT = PRD.prd.LFA1.MANDT AND PRD.prd.VTTK.TDLNR = PRD.prd.LFA1.LIFNR LEFT OUTER JOIN
                                   PRD.prd.TVROT ON PRD.prd.LIKP.MANDT = PRD.prd.TVROT.MANDT AND PRD.prd.LIKP.ROUTE = PRD.prd.TVROT.ROUTE
                      WHERE       (PRD.prd.VBFA.MANDT = '100') AND (PRD.prd.VBFA.VBELV = '" + orderNumber + "') AND (PRD.prd.VBFA.VBTYP_N = 'J') " +
                      "GROUP BY     PRD.prd.VBFA.VBELN, PRD.prd.LIKP.VBELN, PRD.prd.LIKP.ERDAT, PRD.prd.LIKP.BOLNR, PRD.prd.LIKP.ROUTE, PRD.prd.VTTK.TDLNR, PRD.prd.LFA1.NAME1, PRD.prd.TVROT.BEZEI";

            SqlConnection conn = new SqlConnection("Data Source=Jed;Initial Catalog=PRD;User ID=spongebob;Password=benjamin");
            DataSet dataSet = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter(sql, conn);
            if (conn.State == ConnectionState.Closed)
                conn.Open();
            adapter.Fill(dataSet);
            conn.Close();

            order.PackingSlips = new List<SAPModels.PackingSlip>();
            if (dataSet.Tables.Count > 0)
            {
                DataTable table = dataSet.Tables[0];
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    order.PackingSlips.Add(new SAPModels.PackingSlip()
                    {
                        Carrier = table.Rows[i]["NAME1"].ToString(),
                        TrackingNumber = table.Rows[i]["BOLNR"].ToString(),
                        TrackingLink = TrackingFactory.GetLink(table.Rows[i]["BOLNR"].ToString(), table.Rows[i]["TDLNR"].ToString()),
                        DeliveryDate = table.Rows[i]["ERDAT"].ToString(),
                        DeliveryNumber = table.Rows[i]["VBELN"].ToString(),
                    });
                }
            }

            _userActivityRepo.Add(new UserActivity()
            {
                User = User.Identity.Name,
                Action = "View Order",
                Message = "Viewed order: " + orderNumber
            });
            return View(order);
        }

        public ActionResult PrintPackingSlip(string dlvNumber)
        {
            _userActivityRepo.Add(new UserActivity()
            {
                User = User.Identity.Name,
                Action = "Packing Slip Print",
                Message = "Printed packing slip: " + dlvNumber
            });

            string pdf = new OrderService().PrintPackingSlip(dlvNumber);
            return File(@"\\fred\reprint\" + pdf, "application/pdf", pdf);
        }
    }
}
