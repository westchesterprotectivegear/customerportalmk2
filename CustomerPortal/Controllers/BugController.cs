﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CustomerPortalModels;
using System.Web.Security;
using CustomerPortal.Models;

namespace CustomerPortal.Controllers
{
    [Authorize]
    public class BugController : _ApplicationController
    {
        private readonly IBugRepository _bugRepo;
        private readonly IBugCommentRepository _bugCommentRepo;
        private readonly IUserActivityRepository _userActivityRepo;

        public BugController(IBugRepository bugRepo, IBugCommentRepository bugCommentRepo, IUserActivityRepository userActivityRepo)
        {
            _bugRepo = bugRepo;
            _bugCommentRepo = bugCommentRepo;
            _userActivityRepo = userActivityRepo;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Report()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Report(AddBugModel model)
        {
            if (ModelState.IsValid)
            {
                var bug = new Bug()
                {
                    Title = model.Title,
                    UserName = GetUser().UserName,
                    Status = "Open"
                };
                _bugRepo.Add(bug);

                _userActivityRepo.Add(new UserActivity()
                {
                    User = User.Identity.Name,
                    Action = "Bug",
                    Message = "Reported a bug: " + bug.Title
                });

                EmailService.SendMessage("Bug Report", string.Format("A user has submitted a bug: \"{0}\".\n\nPlease view the bug here:\nhttp://customer.westchestergear.com/Bug/Discussion?bugID={1}", bug.Title, bug.ID));

                return RedirectToAction("Discussion", new { bugID = bug.ID });
            }
            else
            {
                ModelState.AddModelError("", "You must enter a title for your bug!");
                return View(model);
            }
        }

        public ActionResult BugList()
        {
            if (Roles.IsUserInRole("Administrator"))
            {
                var bugs = _bugRepo.GetAllOpen();
                return PartialView(bugs);
            }
            else
            {
                var bugs = _bugRepo.GetAll(new NCommon.Specifications.Specification<Bug>(x => x.UserName == GetUser().UserName));
                return PartialView(bugs);
            }
        }

        public ActionResult Discussion(Guid bugID)
        {
            var bug = _bugRepo.Get(bugID);
            return View(bug);
        }

        [HttpPost]
        public ActionResult AddComment(Guid ID, string comment)
        {
            if (!string.IsNullOrEmpty(comment))
            {
                var bugComment = new BugComment()
                {
                    BugID = ID,
                    Text = comment,
                    UserName = GetUser().UserName
                };
                _bugCommentRepo.Add(bugComment);

                var bug = _bugRepo.Get(ID);
                if (User.IsInRole("Administrator"))
                    EmailService.SendMessage("Bug Report Reply", string.Format("A reply to your report on the bug \"{0}\" has been made.\n\nPlease view the response here:\nhttp://customer.westchestergear.com/Bug/Discussion?bugID={1}", bug.Title, bug.ID), bug.UserName);
                else
                    EmailService.SendMessage("Bug Report Reply", string.Format("A user has commented on the bug \"{0}\" has been made.\n\nPlease view the response here:\nhttp://customer.westchestergear.com/Bug/Discussion?bugID={1}", bug.Title, bug.ID));
            }
            return RedirectToAction("Discussion", new { bugID = ID });
        }

        [HttpPost]
        public ActionResult Close(Guid ID)
        {
            _bugRepo.Close(ID);
            return RedirectToAction("Discussion", new { bugID = ID });
        }

        [HttpPost]
        public ActionResult Open(Guid ID)
        {
            _bugRepo.Open(ID);
            return RedirectToAction("Discussion", new { bugID = ID });
        }
    }
}
