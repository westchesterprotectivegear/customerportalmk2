﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CustomerPortalModels;
using CustomerPortal.Models;

namespace CustomerPortal.Controllers
{
    [Authorize]
    public class ProductController : _ApplicationController
    {
        private readonly IProductRepository _productRepo;
        private readonly IFavoriteItemRepository _favRepo;
        private readonly IUserActivityRepository _userActivityRepo;

        public ProductController(IProductRepository productRepo, IFavoriteItemRepository favRepo, IUserActivityRepository userActivityRepo)
        {
            _productRepo = productRepo;
            _favRepo = favRepo;
            _userActivityRepo = userActivityRepo;
        }

        public ActionResult Index()
        {
            var customerNumber = GetUser().CustomerNumber;
            var products = _productRepo.GetAll(customerNumber).Take(100).ToList();
            return View(products);
        }

        [HttpPost]
        public ActionResult Index(string searchData)
        {
            var customerNumber = GetUser().CustomerNumber;
            var products = _productRepo.GetAll(customerNumber).Where(x => x.SKU.ToUpper().Contains(searchData.ToUpper()) || x.NonNullDescription.ToUpper().Contains(searchData.ToUpper())).Take(100).ToList();
            return View(products);
        }

        [HttpPost]
        public ActionResult AdvancedSearch(ProductAdvancedSearchModel model)
        {
            var products = _productRepo.GetAll(GetUser().CustomerNumber);

            if (!string.IsNullOrEmpty(model.SKU))
                products = products.Where(x => x.SKU.Contains(model.SKU));

            if (!string.IsNullOrEmpty(model.CustomerSKU))
                products = products.Where(x => x.CustomerSKU.Contains(model.CustomerSKU));

            if (!string.IsNullOrEmpty(model.Description))
                products = products.Where(x => x.NonNullDescription.Contains(model.Description));

            if (model.PriceLow != 0.0)
                products = products.Where(x => x.Price >= model.PriceLow);

            if (model.PriceHigh != 0.0)
                products = products.Where(x => x.Price <= model.PriceHigh);

            if (products.Count() == 0)
            {
                ViewData["emptyMessage"] = "Sorry! No results match your search terms.";
            }

            return View("Index", products.ToList());
        }

        public ActionResult ViewProduct(string sku)
        {
            var customerNumber = GetUser().CustomerNumber;
            var product = _productRepo.Get(sku, customerNumber);

            var model = new ProductViewModel()
            {
                Product = product,
                RelatedProducts = _productRepo.GetAll(customerNumber).Where(x => x.BasePartNumber == product.BasePartNumber && x.SKU != product.SKU).ToList(),
                IsFavorite = _favRepo.Exists(sku, GetUser().UserName)
            };

            _userActivityRepo.Add(new UserActivity()
            {
                User = User.Identity.Name,
                Action = "View Product",
                Message = "Viewed " + sku
            });

            return View(model);
        }

        [HttpPost]
        public ActionResult ToggleFavorite(string sku)
        {
            var userName = GetUser().UserName;
            Message message;

            if (_favRepo.Exists(sku, userName))
            {
                _favRepo.Remove(sku, userName);

                _userActivityRepo.Add(new UserActivity()
                {
                    User = User.Identity.Name,
                    Action = "Favorite",
                    Message = "Removed " + sku + " from favorites"
                });

                message = new Message()
                {
                    Type = "success",
                    Content = sku + " has been removed from your favorites"
                };
            }
            else
            {
                var fav = new FavoriteItem()
                {
                    UserName = GetUser().UserName,
                    SKU = sku
                };

                _favRepo.Add(fav);

                _userActivityRepo.Add(new UserActivity()
                {
                    User = User.Identity.Name,
                    Action = "Favorite",
                    Message = "Added " + sku + " to favorites"
                });

                message = new Message()
                {
                    Type = "success",
                    Content = sku + " has been added to your favorites"
                };
            }

            return Json(new { message = message });
        }

        public ActionResult FavoriteList()
        {
            var favs = _favRepo.GetAll(GetUser().UserName).Take(100).ToList();
            var products = _productRepo.GetAll((from x in favs select x.SKU), GetUser().CustomerNumber).ToList();
            return View(products);
        }

        [HttpPost]
        public ActionResult FavoriteList(string searchData)
        {
            var favs = _favRepo.GetAll(GetUser().UserName).Where(x => x.SKU.Contains(searchData)).Take(100).ToList();
            var products = _productRepo.GetAll((from x in favs select x.SKU), GetUser().CustomerNumber).ToList();
            return View(products);
        }

        public ActionResult FavoritesTile()
        {
            try
            {
                var favs = _favRepo.GetAll(GetUser().UserName).Take(5).ToList();
                var products = _productRepo.GetAll((from x in favs select x.SKU), GetUser().CustomerNumber);
                return PartialView(products);
            }
            catch (Exception)
            {
                return PartialView();
            }
        }

        public JsonResult AutoCompleteSKU(string term)
        {
            var result = (from x in _productRepo.GetAll(GetUser().CustomerNumber) where x.SKU.Contains(term) select x.SKU).Distinct();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}
