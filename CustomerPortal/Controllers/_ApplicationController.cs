﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using CustomerPortalModels;
using System.IO;
using Ninject;
using NCommon.Specifications;
using CustomerPortal;

namespace CustomerPortal.Controllers
{
    public class _ApplicationController : Controller
    {
        [Inject]
        public IUnitOfWork<CustomerPortalDatabaseContext> UnitOfWork { get; set; }
        [Inject]
        public IRepository<User, CustomerPortalDatabaseContext> userRepo { get; set; }

        public User GetUser()
        {
            var user = (User)Session["user"];
            if (user != null && !string.IsNullOrWhiteSpace(user.CustomerNumber))
                user.CustomerNumber = user.CustomerNumber.PadSAP(10);
            return user;
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {
                if (!filterContext.IsChildAction)
                    UnitOfWork.BeginTransaction();

                if (User.Identity.IsAuthenticated)
                {
                    if (Session["user"] == null)
                        Session["user"] = userRepo.Get(new Specification<User>(x => x.UserName == User.Identity.Name));
                }
            }
            catch
            {
                Session.Clear();
                FormsAuthentication.SignOut();
                Redirect("http://localhost:51033/Account/LogOff");
            }
        }

        protected override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            if (!filterContext.IsChildAction)
                UnitOfWork.Commit();
        }

        public string RenderViewToString(string viewName, object model)
        {
            ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext,
                                                                         viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View,
                                             ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }

    }
}
