﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using WebMatrix.WebData;
using CustomerPortal.Models;
using CustomerPortalModels;
using NCommon.Specifications;
using Ninject;

namespace CustomerPortal.Controllers
{
    [Authorize]
    public class AccountController : _ApplicationController
    {
        private readonly IAccountRequestRepository _accountRequestRepo;
        private readonly ICustomerRepository _customerRepo;
        private readonly IUserActivityRepository _userActivityRepo;
        private readonly IPasswordResetRepository _passwordResetRepo;

        public AccountController(IAccountRequestRepository accountRequestRepo, ICustomerRepository customerRepo, IUserActivityRepository userActivityRepo, IPasswordResetRepository passwordResetRepo)
        {
            _accountRequestRepo = accountRequestRepo;
            _customerRepo = customerRepo;
            _userActivityRepo = userActivityRepo;
            _passwordResetRepo = passwordResetRepo;
        }

        //
        // GET: /Account/Login

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            if (ModelState.IsValid && WebSecurity.Login(model.UserName, model.Password, persistCookie: model.RememberMe))
            {
                _userActivityRepo.Add(new UserActivity()
                {
                    User = model.UserName,
                    Action = "Login",
                    Message = model.UserName + " logged in"
                });

                return RedirectToLocal(returnUrl);
            }

            // If we got this far, something failed, redisplay form
            ModelState.AddModelError("", "The user name or password provided is incorrect.");
            return View(model);
        }

        //
        // POST: /Account/LogOff

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            WebSecurity.Logout();
            Session.Clear();
            return RedirectToAction("Index", "Home");
        }


        [HttpGet]
        [AllowAnonymous]
        public ActionResult RequestAccount()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult RequestAccount(RequestAccountModel requestAccountModel)
        {
            if (!string.IsNullOrEmpty(requestAccountModel.Email))
            {
                requestAccountModel.Email = requestAccountModel.Email.ToLower();
            }
            if (ModelState.IsValid)
            {
                if (_accountRequestRepo.Exists(requestAccountModel.Email))
                {
                    var request = _accountRequestRepo.Get(requestAccountModel.Email);
                    string message = "A request has already been made for this account. ";

                    switch (request.Status)
                    {
                        case "Pending":
                            message += "The request is still pending approval. You will be notified upon approval of your request.";
                            break;
                        case "Denied":
                            message += "The request has been denied. Please contact your account manager if you believe this is an error.";
                            break;
                        case "Approved":
                            message += "The request has been Approved. An email has been sent to the address associated with this account. Please follow the instructions in the email to reset your password and access your account. Thank you.";

                            var key = Guid.NewGuid();
                            _passwordResetRepo.Add(new PasswordReset()
                            {
                                PrivateKey = key,
                                UserName = requestAccountModel.Email
                            });

                            EmailService.SendMessage("Reset Your Password", "Use the link below to reset your password:\n\n http://customer.westchestergear.com/Account/ResetPassword?key=" + key, requestAccountModel.Email);
                            break;
                    }

                    ViewData["Error"] = message;
                    return View("RequestAccountCompleted");
                }
                else
                {
                    AccountRequest request = new AccountRequest()
                    {
                        Email = requestAccountModel.Email,
                        Name = requestAccountModel.Name,
                        Company = requestAccountModel.CompanyName,
                        Phone = requestAccountModel.TelephoneNumber,
                        Street = requestAccountModel.Street,
                        City = requestAccountModel.City,
                        State = requestAccountModel.State,
                        Zip = requestAccountModel.Zip,
                        MailingList = requestAccountModel.ReceiveMarketingEmails,
                        Date = DateTime.Now,
                        Status = "Pending"
                    };

                    _accountRequestRepo.Add(request);
                    return View("RequestAccountCompleted");
                }
            }

            return View("RequestAccount", requestAccountModel);
        }

        public ActionResult CreateAccountFromRequest(CreateAccountFromRequestModel model)
        {
            var acct = _accountRequestRepo.Get(model.Email);

            var header = string.Format("{0}\n{1}\n{2}\n{3} {4} {5}", 
                acct.Name, acct.Company, acct.Street, acct.City, acct.State, acct.Zip);
            if (model.AccountType == "Customer")
            {
                var custModel = new RegisterCustomerModel()
                {
                    UserName = acct.Email,
                    DisplayName = acct.Name,
                    Active = true,
                    UseTools = true,
                    IsRequest = true,
                    Header = header
                };

                return View("RegisterCustomer", custModel);
            }
            else
            {
                var repModel = new RegisterRepGroupModel()
                {
                    UserName = acct.Email,
                    DisplayName = acct.Name,
                    Active = true,
                    UseTools = true,
                    IsRequest = true,
                    Header = header
                };

                return View("RegisterRepGroup", repModel);
            }
        }

        public ActionResult AccountRequests()
        {
            var orders = _accountRequestRepo.GetAll().Where(x => x.Status == "Pending").Take(100).ToList();
            return View(orders);
        }

        [HttpPost]
        public ActionResult AccountRequests(string searchData)
        {
            searchData = searchData.ToLower();
            var orders = _accountRequestRepo.GetAll().Where(x => x.Status == "Pending" && (x.Email.ToLower().Contains(searchData) || x.Name.ToLower().Contains(searchData))).Take(100).ToList();
            return View(orders);
        }

        //
        // GET: /Account/Register

        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        //
        // POST: /Account/Register


        public ActionResult RegisterAdministrator()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RegisterAdministrator(RegisterUserModel model)
        {
            if (ModelState.IsValid)
            {
                // Attempt to register the user
                try
                {
                    WebSecurity.CreateUserAndAccount(model.UserName, model.Password);
                    InternalUser user = new InternalUser()
                    {
                        UserName = model.UserName,
                        DisplayName = model.DisplayName
                    };

                    userRepo.Create(user);

                    Roles.AddUserToRole(user.UserName, "PowerUser");
                    Roles.AddUserToRole(user.UserName, "Administrator");
                    Roles.AddUserToRole(user.UserName, "Internal");

                    Roles.AddUserToRole(user.UserName, "CreateUsers");
                    Roles.AddUserToRole(user.UserName, "ViewAllShipTos");
                    Roles.AddUserToRole(user.UserName, "RequestSamples");
                    Roles.AddUserToRole(user.UserName, "PlaceOrders");
                    Roles.AddUserToRole(user.UserName, "PayInvoices");
                    Roles.AddUserToRole(user.UserName, "ViewPricing");
                    Roles.AddUserToRole(user.UserName, "ViewOrders");
                    Roles.AddUserToRole(user.UserName, "UseTools");
                    Roles.AddUserToRole(user.UserName, "Active");

                    TempData["registerAccountMessage"] = new Message()
                    {
                        Type = "success",
                        Content = "The account for " + model.DisplayName + " has been successfully created."
                    };

                    return RedirectToAction("Index", "Home");
                }
                catch (MembershipCreateUserException e)
                {
                    ModelState.AddModelError("", ErrorCodeToString(e.StatusCode));
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        public ActionResult RegisterPowerUser()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RegisterPowerUser(RegisterUserModel model)
        {
            if (ModelState.IsValid)
            {
                // Attempt to register the user
                try
                {
                    WebSecurity.CreateUserAndAccount(model.UserName, model.Password);
                    InternalUser user = new InternalUser()
                    {
                        UserName = model.UserName,
                        DisplayName = model.DisplayName
                    };

                    userRepo.Create(user);

                    Roles.AddUserToRole(user.UserName, "PowerUser");
                    Roles.AddUserToRole(user.UserName, "Internal");

                    Roles.AddUserToRole(user.UserName, "CreateUsers");
                    Roles.AddUserToRole(user.UserName, "ViewAllShipTos");
                    Roles.AddUserToRole(user.UserName, "RequestSamples");
                    Roles.AddUserToRole(user.UserName, "PlaceOrders");
                    Roles.AddUserToRole(user.UserName, "PayInvoices");
                    Roles.AddUserToRole(user.UserName, "ViewPricing");
                    Roles.AddUserToRole(user.UserName, "ViewOrders");
                    Roles.AddUserToRole(user.UserName, "UseTools");
                    Roles.AddUserToRole(user.UserName, "Active");

                    TempData["registerAccountMessage"] = new Message()
                    {
                        Type = "success",
                        Content = "The account for " + model.DisplayName + " has been successfully created."
                    };

                    return RedirectToAction("Index", "Home");
                }
                catch (MembershipCreateUserException e)
                {
                    ModelState.AddModelError("", ErrorCodeToString(e.StatusCode));
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        public ActionResult RegisterInternalUser()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RegisterInternalUser(RegisterUserModel model)
        {
            if (ModelState.IsValid)
            {
                // Attempt to register the user
                try
                {
                    WebSecurity.CreateUserAndAccount(model.UserName, model.Password);
                    InternalUser user = new InternalUser()
                    {
                        UserName = model.UserName,
                        DisplayName = model.DisplayName
                    };

                    userRepo.Create(user);

                    Roles.AddUserToRole(user.UserName, "Internal");

                    Roles.AddUserToRole(user.UserName, "ViewAllShipTos");
                    Roles.AddUserToRole(user.UserName, "RequestSamples");
                    Roles.AddUserToRole(user.UserName, "PlaceOrders");
                    Roles.AddUserToRole(user.UserName, "PayInvoices");
                    Roles.AddUserToRole(user.UserName, "ViewPricing");
                    Roles.AddUserToRole(user.UserName, "ViewOrders");
                    Roles.AddUserToRole(user.UserName, "UseTools");
                    Roles.AddUserToRole(user.UserName, "Active");

                    TempData["registerAccountMessage"] = new Message()
                    {
                        Type = "success",
                        Content = "The account for " + model.DisplayName + " has been successfully created."
                    };

                    return RedirectToAction("Index", "Home");
                }
                catch (MembershipCreateUserException e)
                {
                    ModelState.AddModelError("", ErrorCodeToString(e.StatusCode));
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        public ActionResult RegisterCustomer()
        {
            var model = new RegisterCustomerModel()
            {
                Active = true,
                UseTools = true
            };
            return View(model);
        }

        public ActionResult Edit(string userName)
        {
            var user = userRepo.Get(new Specification<User>(x => x.UserName == userName));

            if (user is CustomerUser)
            {
                var customer = (CustomerUser)user;
                var model = new EditCustomerModel();
                model.UserName = customer.UserName;
                model.DisplayName = customer.DisplayName;
                model.CustomerNumber = customer.CustomerNumber;
                model.DefaultShipTo = customer.DefaultShipTo;
                model.SalesRep = customer.SalesRep;
                model.AccountManager = customer.AccountManager;

                model.SeeShipTos = Roles.IsUserInRole(model.UserName, "ViewAllShipTos");
                model.RequestSamples = Roles.IsUserInRole(model.UserName, "RequestSamples");
                model.PlaceOrders = Roles.IsUserInRole(model.UserName, "PlaceOrders");
                model.PayInvoices = Roles.IsUserInRole(model.UserName, "PayInvoices");
                model.SeePricing = Roles.IsUserInRole(model.UserName, "ViewPricing");
                model.ViewOrders = Roles.IsUserInRole(model.UserName, "ViewOrders");
                model.TelesalesAccount = Roles.IsUserInRole(model.UserName, "Telesales");
                model.UseTools = Roles.IsUserInRole(model.UserName, "UseTools");
                model.Active = Roles.IsUserInRole(model.UserName, "Active");

                return View("EditCustomer", model);
            }
            else if (user is RepGroupUser)
            {
                var rep = (RepGroupUser)user;
                var model = new EditRepGroupModel();
                model.UserName = rep.UserName;
                model.DisplayName = rep.DisplayName;
                model.RepGroupNumber = rep.RepGroupNumber;
                model.DefaultShipTo = rep.DefaultShipTo;
                model.SalesRep = rep.SalesRep;
                model.AccountManager = rep.AccountManager;

                model.SeeShipTos = Roles.IsUserInRole(model.UserName, "ViewAllShipTos");
                model.RequestSamples = Roles.IsUserInRole(model.UserName, "RequestSamples");
                model.PlaceOrders = Roles.IsUserInRole(model.UserName, "PlaceOrders");
                model.PayInvoices = Roles.IsUserInRole(model.UserName, "PayInvoices");
                model.SeePricing = Roles.IsUserInRole(model.UserName, "ViewPricing");
                model.ViewOrders = Roles.IsUserInRole(model.UserName, "ViewOrders");
                model.TelesalesAccount = Roles.IsUserInRole(model.UserName, "Telesales");
                model.UseTools = Roles.IsUserInRole(model.UserName, "UseTools");
                model.Active = Roles.IsUserInRole(model.UserName, "Active");

                return View("EditRepGroup", model);
            }
            else if (user is InternalUser)
            {
                var customer = (InternalUser)user;
                var model = new EditUserModel();
                model.UserName = customer.UserName;
                model.DisplayName = customer.DisplayName;

                model.SeeShipTos = Roles.IsUserInRole(model.UserName, "ViewAllShipTos");
                model.RequestSamples = Roles.IsUserInRole(model.UserName, "RequestSamples");
                model.PlaceOrders = Roles.IsUserInRole(model.UserName, "PlaceOrders");
                model.PayInvoices = Roles.IsUserInRole(model.UserName, "PayInvoices");
                model.SeePricing = Roles.IsUserInRole(model.UserName, "ViewPricing");
                model.ViewOrders = Roles.IsUserInRole(model.UserName, "ViewOrders");
                model.UseTools = Roles.IsUserInRole(model.UserName, "UseTools");
                model.Active = Roles.IsUserInRole(model.UserName, "Active");
                model.CreateUsers = Roles.IsUserInRole(model.UserName, "CreateUsers");

                return View("EditInternalUser", model);
            }
            return View();
        }

        public ActionResult EditCustomer(string userName)
        {
            var user = (CustomerUser)userRepo.Get(new Specification<User>(x => x.UserName == userName));
            var model = new EditCustomerModel();
            model.UserName = user.UserName;
            model.DisplayName = user.DisplayName;
            model.CustomerNumber = user.CustomerNumber;
            model.DefaultShipTo = user.DefaultShipTo;
            model.SalesRep = user.SalesRep;
            model.AccountManager = user.AccountManager;

            model.SeeShipTos = Roles.IsUserInRole(model.UserName, "ViewAllShipTos");
            model.RequestSamples = Roles.IsUserInRole(model.UserName, "RequestSamples");
            model.PlaceOrders = Roles.IsUserInRole(model.UserName, "PlaceOrders");
            model.PayInvoices = Roles.IsUserInRole(model.UserName, "PayInvoices");
            model.SeePricing = Roles.IsUserInRole(model.UserName, "ViewPricing");
            model.ViewOrders = Roles.IsUserInRole(model.UserName, "ViewOrders");
            model.TelesalesAccount = Roles.IsUserInRole(model.UserName, "Telesales");
            model.UseTools = Roles.IsUserInRole(model.UserName, "UseTools");
            model.Active = Roles.IsUserInRole(model.UserName, "Active");

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RegisterCustomer(RegisterCustomerModel model)
        {
            if (ModelState.IsValid)
            {
                // Attempt to register the user
                try
                {
                    // random password
                    var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                    var random = new Random();
                    var password = new string(
                        Enumerable.Repeat(chars, 8)
                                  .Select(s => s[random.Next(s.Length)])
                                  .ToArray());

                    WebSecurity.CreateUserAndAccount(model.UserName, password);
                    CustomerUser user = new CustomerUser()
                    {
                        UserName = model.UserName,
                        DisplayName = model.DisplayName,
                        CustomerNumber = model.CustomerNumber.ToUpper(),
                        DefaultShipTo = model.DefaultShipTo,
                        SalesRep = model.SalesRep,
                        AccountManager = model.AccountManager
                    };

                    if (model.SeeShipTos)
                        Roles.AddUserToRole(user.UserName, "ViewAllShipTos");
                    if (model.RequestSamples)
                        Roles.AddUserToRole(user.UserName, "RequestSamples");
                    if (model.PlaceOrders)
                        Roles.AddUserToRole(user.UserName, "PlaceOrders");
                    if (model.PayInvoices)
                        Roles.AddUserToRole(user.UserName, "PayInvoices");
                    if (model.SeePricing)
                        Roles.AddUserToRole(user.UserName, "ViewPricing");
                    if (model.ViewOrders)
                        Roles.AddUserToRole(user.UserName, "ViewOrders");
                    if (model.TelesalesAccount)
                        Roles.AddUserToRole(user.UserName, "Telesales");
                    if (model.UseTools)
                        Roles.AddUserToRole(user.UserName, "UseTools");
                    if (model.Active)
                        Roles.AddUserToRole(user.UserName, "Active");

                    Roles.AddUserToRole(user.UserName, "Customer");

                    var key = Guid.NewGuid();
                    _passwordResetRepo.Add(new PasswordReset()
                    {
                        PrivateKey = key,
                        UserName = model.UserName
                    });

                    userRepo.Create(user);

                    EmailService.SendMessage("Activate Your WCPG Customer Portal Acccount", "Use the link below to create a password and activate your account:\n\n http://customer.westchestergear.com/Account/ActivateAccount?key=" + key, model.UserName);

                    TempData["registerAccountMessage"] = new Message()
                    {
                        Type = "success",
                        Content = "The account for " + model.DisplayName + " has been successfully created."
                    };

                    var request = _accountRequestRepo.Get(model.UserName);
                    if (request != null)
                    {
                        request.Status = "Approved";
                        _accountRequestRepo.Save(request);
                    }

                    return RedirectToAction("Index", "Home");
                }
                catch (MembershipCreateUserException e)
                {
                    ModelState.AddModelError("", ErrorCodeToString(e.StatusCode));
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditCustomer(EditCustomerModel model)
        {
            if (ModelState.IsValid)
            {
                // Attempt to register the user
                try
                {
                    var user = (CustomerUser)userRepo.Get(new Specification<User>(x => x.UserName == model.UserName));
                    user.UserName = model.UserName;
                    user.DisplayName = model.DisplayName;
                    user.CustomerNumber = model.CustomerNumber.ToUpper();
                    user.DefaultShipTo = model.DefaultShipTo;
                    user.SalesRep = model.SalesRep;
                    user.AccountManager = model.AccountManager;

                    if (Roles.IsUserInRole(model.UserName, "ViewAllShipTos"))
                        Roles.RemoveUserFromRole(user.UserName, "ViewAllShipTos");
                    if (Roles.IsUserInRole(model.UserName, "RequestSamples"))
                        Roles.RemoveUserFromRole(user.UserName, "RequestSamples");
                    if (Roles.IsUserInRole(model.UserName, "PlaceOrders"))
                        Roles.RemoveUserFromRole(user.UserName, "PlaceOrders");
                    if (Roles.IsUserInRole(model.UserName, "PayInvoices"))
                        Roles.RemoveUserFromRole(user.UserName, "PayInvoices");
                    if (Roles.IsUserInRole(model.UserName, "ViewPricing"))
                        Roles.RemoveUserFromRole(user.UserName, "ViewPricing");
                    if (Roles.IsUserInRole(model.UserName, "ViewOrders"))
                        Roles.RemoveUserFromRole(user.UserName, "ViewOrders");
                    if (Roles.IsUserInRole(model.UserName, "Telesales"))
                        Roles.RemoveUserFromRole(user.UserName, "Telesales");
                    if (Roles.IsUserInRole(model.UserName, "UseTools"))
                        Roles.RemoveUserFromRole(user.UserName, "UseTools");
                    if (Roles.IsUserInRole(model.UserName, "Active"))
                        Roles.RemoveUserFromRole(user.UserName, "Active");

                    if (model.SeeShipTos)
                        Roles.AddUserToRole(user.UserName, "ViewAllShipTos");
                    if (model.RequestSamples)
                        Roles.AddUserToRole(user.UserName, "RequestSamples");
                    if (model.PlaceOrders)
                        Roles.AddUserToRole(user.UserName, "PlaceOrders");
                    if (model.PayInvoices)
                        Roles.AddUserToRole(user.UserName, "PayInvoices");
                    if (model.SeePricing)
                        Roles.AddUserToRole(user.UserName, "ViewPricing");
                    if (model.ViewOrders)
                        Roles.AddUserToRole(user.UserName, "ViewOrders");
                    if (model.TelesalesAccount)
                        Roles.AddUserToRole(user.UserName, "Telesales");
                    if (model.UseTools)
                        Roles.AddUserToRole(user.UserName, "UseTools");
                    if (model.Active)
                        Roles.AddUserToRole(user.UserName, "Active");

                    userRepo.Update(user);

                    TempData["registerAccountMessage"] = new Message()
                    {
                        Type = "success",
                        Content = "The changes for " + model.DisplayName + " have been saved."
                    };

                    return RedirectToAction("Index", "Home");
                }
                catch (MembershipCreateUserException e)
                {
                    ModelState.AddModelError("", ErrorCodeToString(e.StatusCode));
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditRepGroup(EditRepGroupModel model)
        {
            if (ModelState.IsValid)
            {
                // Attempt to register the user
                try
                {
                    var user = (RepGroupUser)userRepo.Get(new Specification<User>(x => x.UserName == model.UserName));
                    user.UserName = model.UserName;
                    user.DisplayName = model.DisplayName;
                    user.CustomerNumber = model.RepGroupNumber.ToUpper();
                    user.DefaultShipTo = model.DefaultShipTo;
                    user.SalesRep = model.SalesRep;
                    user.AccountManager = model.AccountManager;

                    if (Roles.IsUserInRole(model.UserName, "ViewAllShipTos"))
                        Roles.RemoveUserFromRole(user.UserName, "ViewAllShipTos");
                    if (Roles.IsUserInRole(model.UserName, "RequestSamples"))
                        Roles.RemoveUserFromRole(user.UserName, "RequestSamples");
                    if (Roles.IsUserInRole(model.UserName, "PlaceOrders"))
                        Roles.RemoveUserFromRole(user.UserName, "PlaceOrders");
                    if (Roles.IsUserInRole(model.UserName, "PayInvoices"))
                        Roles.RemoveUserFromRole(user.UserName, "PayInvoices");
                    if (Roles.IsUserInRole(model.UserName, "ViewPricing"))
                        Roles.RemoveUserFromRole(user.UserName, "ViewPricing");
                    if (Roles.IsUserInRole(model.UserName, "ViewOrders"))
                        Roles.RemoveUserFromRole(user.UserName, "ViewOrders");
                    if (Roles.IsUserInRole(model.UserName, "Telesales"))
                        Roles.RemoveUserFromRole(user.UserName, "Telesales");
                    if (Roles.IsUserInRole(model.UserName, "UseTools"))
                        Roles.RemoveUserFromRole(user.UserName, "UseTools");
                    if (Roles.IsUserInRole(model.UserName, "Active"))
                        Roles.RemoveUserFromRole(user.UserName, "Active");

                    if (model.SeeShipTos)
                        Roles.AddUserToRole(user.UserName, "ViewAllShipTos");
                    if (model.RequestSamples)
                        Roles.AddUserToRole(user.UserName, "RequestSamples");
                    if (model.PlaceOrders)
                        Roles.AddUserToRole(user.UserName, "PlaceOrders");
                    if (model.PayInvoices)
                        Roles.AddUserToRole(user.UserName, "PayInvoices");
                    if (model.SeePricing)
                        Roles.AddUserToRole(user.UserName, "ViewPricing");
                    if (model.ViewOrders)
                        Roles.AddUserToRole(user.UserName, "ViewOrders");
                    if (model.TelesalesAccount)
                        Roles.AddUserToRole(user.UserName, "Telesales");
                    if (model.UseTools)
                        Roles.AddUserToRole(user.UserName, "UseTools");
                    if (model.Active)
                        Roles.AddUserToRole(user.UserName, "Active");

                    userRepo.Update(user);

                    TempData["registerAccountMessage"] = new Message()
                    {
                        Type = "success",
                        Content = "The changes for " + model.DisplayName + " have been saved."
                    };

                    return RedirectToAction("Index", "Home");
                }
                catch (MembershipCreateUserException e)
                {
                    ModelState.AddModelError("", ErrorCodeToString(e.StatusCode));
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditInternalUser(EditUserModel model)
        {
            if (ModelState.IsValid)
            {
                // Attempt to register the user
                try
                {
                    var user = (InternalUser)userRepo.Get(new Specification<User>(x => x.UserName == model.UserName));
                    user.UserName = model.UserName;
                    user.DisplayName = model.DisplayName;

                    if (Roles.IsUserInRole(model.UserName, "CreateUsers"))
                        Roles.RemoveUserFromRole(user.UserName, "CreateUsers");
                    if (Roles.IsUserInRole(model.UserName, "ViewAllShipTos"))
                        Roles.RemoveUserFromRole(user.UserName, "ViewAllShipTos");
                    if (Roles.IsUserInRole(model.UserName, "RequestSamples"))
                        Roles.RemoveUserFromRole(user.UserName, "RequestSamples");
                    if (Roles.IsUserInRole(model.UserName, "PlaceOrders"))
                        Roles.RemoveUserFromRole(user.UserName, "PlaceOrders");
                    if (Roles.IsUserInRole(model.UserName, "PayInvoices"))
                        Roles.RemoveUserFromRole(user.UserName, "PayInvoices");
                    if (Roles.IsUserInRole(model.UserName, "ViewPricing"))
                        Roles.RemoveUserFromRole(user.UserName, "ViewPricing");
                    if (Roles.IsUserInRole(model.UserName, "ViewOrders"))
                        Roles.RemoveUserFromRole(user.UserName, "ViewOrders");
                    if (Roles.IsUserInRole(model.UserName, "Telesales"))
                        Roles.RemoveUserFromRole(user.UserName, "Telesales");
                    if (Roles.IsUserInRole(model.UserName, "UseTools"))
                        Roles.RemoveUserFromRole(user.UserName, "UseTools");
                    if (Roles.IsUserInRole(model.UserName, "Active"))
                        Roles.RemoveUserFromRole(user.UserName, "Active");

                    if (model.CreateUsers)
                        Roles.AddUserToRole(user.UserName, "CreateUsers");
                    if (model.SeeShipTos)
                        Roles.AddUserToRole(user.UserName, "ViewAllShipTos");
                    if (model.RequestSamples)
                        Roles.AddUserToRole(user.UserName, "RequestSamples");
                    if (model.PlaceOrders)
                        Roles.AddUserToRole(user.UserName, "PlaceOrders");
                    if (model.PayInvoices)
                        Roles.AddUserToRole(user.UserName, "PayInvoices");
                    if (model.SeePricing)
                        Roles.AddUserToRole(user.UserName, "ViewPricing");
                    if (model.ViewOrders)
                        Roles.AddUserToRole(user.UserName, "ViewOrders");
                    if (model.UseTools)
                        Roles.AddUserToRole(user.UserName, "UseTools");
                    if (model.Active)
                        Roles.AddUserToRole(user.UserName, "Active");

                    userRepo.Update(user);

                    TempData["registerAccountMessage"] = new Message()
                    {
                        Type = "success",
                        Content = "The changes for " + model.DisplayName + " have been saved."
                    };

                    return RedirectToAction("Index", "Home");
                }
                catch (MembershipCreateUserException e)
                {
                    ModelState.AddModelError("", ErrorCodeToString(e.StatusCode));
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        public ActionResult RegisterRepGroup()
        {
            var model = new RegisterRepGroupModel()
            {
                Active = true,
                UseTools = true
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RegisterRepGroup(RegisterRepGroupModel model)
        {
            if (ModelState.IsValid)
            {
                // Attempt to register the user
                try
                {
                    // random password
                    var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                    var random = new Random();
                    var password = new string(
                        Enumerable.Repeat(chars, 8)
                                  .Select(s => s[random.Next(s.Length)])
                                  .ToArray());

                    WebSecurity.CreateUserAndAccount(model.UserName, password);
                    RepGroupUser user = new RepGroupUser()
                    {
                        UserName = model.UserName,
                        DisplayName = model.DisplayName,
                        RepGroupNumber = model.RepGroupNumber
                    };

                    userRepo.Create(user);

                    Roles.AddUserToRole(user.UserName, "RepGroup");

                    Roles.AddUserToRole(user.UserName, "ViewAllShipTos");
                    Roles.AddUserToRole(user.UserName, "RequestSamples");
                    Roles.AddUserToRole(user.UserName, "PlaceOrders");
                    Roles.AddUserToRole(user.UserName, "PayInvoices");
                    Roles.AddUserToRole(user.UserName, "ViewPricing");
                    Roles.AddUserToRole(user.UserName, "ViewOrders");
                    Roles.AddUserToRole(user.UserName, "UseTools");
                    Roles.AddUserToRole(user.UserName, "Active");

                    TempData["registerAccountMessage"] = new Message()
                    {
                        Type = "success",
                        Content = "The account for " + model.DisplayName + " has been successfully created."
                    };

                    var request = _accountRequestRepo.Get(model.UserName);
                    if (request != null)
                    {
                        request.Status = "Approved";
                        _accountRequestRepo.Save(request);
                    }

                    return RedirectToAction("Index", "Home");
                }
                catch (MembershipCreateUserException e)
                {
                    ModelState.AddModelError("", ErrorCodeToString(e.StatusCode));
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // POST: /Account/Disassociate

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Disassociate(string provider, string providerUserId)
        {
            string ownerAccount = OAuthWebSecurity.GetUserName(provider, providerUserId);
            ManageMessageId? message = null;

            // Only disassociate the account if the currently logged in user is the owner
            if (ownerAccount == User.Identity.Name)
            {
                // Use a transaction to prevent the user from deleting their last login credential
                using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Serializable }))
                {
                    bool hasLocalAccount = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
                    if (hasLocalAccount || OAuthWebSecurity.GetAccountsFromUserName(User.Identity.Name).Count > 1)
                    {
                        OAuthWebSecurity.DeleteAccount(provider, providerUserId);
                        scope.Complete();
                        message = ManageMessageId.RemoveLoginSuccess;
                    }
                }
            }

            return RedirectToAction("Manage", new { Message = message });
        }

        //
        // GET: /Account/Manage

        public ActionResult Manage(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
                : message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
                : message == ManageMessageId.RemoveLoginSuccess ? "The external login was removed."
                : "";
            ViewBag.HasLocalPassword = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
            ViewBag.ReturnUrl = Url.Action("Manage");
            return View();
        }

        //
        // POST: /Account/Manage

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Manage(LocalPasswordModel model)
        {
            bool hasLocalAccount = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
            ViewBag.HasLocalPassword = hasLocalAccount;
            ViewBag.ReturnUrl = Url.Action("Manage");
            if (hasLocalAccount)
            {
                if (ModelState.IsValid)
                {
                    // ChangePassword will throw an exception rather than return false in certain failure scenarios.
                    bool changePasswordSucceeded;
                    try
                    {
                        changePasswordSucceeded = WebSecurity.ChangePassword(User.Identity.Name, model.OldPassword, model.NewPassword);
                    }
                    catch (Exception)
                    {
                        changePasswordSucceeded = false;
                    }

                    if (changePasswordSucceeded)
                    {
                        return RedirectToAction("Manage", new { Message = ManageMessageId.ChangePasswordSuccess });
                    }
                    else
                    {
                        ModelState.AddModelError("", "The current password is incorrect or the new password is invalid.");
                    }
                }
            }
            else
            {
                // User does not have a local password so remove any validation errors caused by a missing
                // OldPassword field
                ModelState state = ModelState["OldPassword"];
                if (state != null)
                {
                    state.Errors.Clear();
                }

                if (ModelState.IsValid)
                {
                    try
                    {
                        WebSecurity.CreateAccount(User.Identity.Name, model.NewPassword);
                        return RedirectToAction("Manage", new { Message = ManageMessageId.SetPasswordSuccess });
                    }
                    catch (Exception e)
                    {
                        ModelState.AddModelError("", e);
                    }
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        public ActionResult History()
        {
            return View();
        }

        public ActionResult ActivityList()
        {
            var list = _userActivityRepo.GetAll(User.Identity.Name);
            return PartialView(list.OrderByDescending(x => x.Date));
        }

        [Authorize(Roles = "Administrator, PowerUser, Internal")]
        public ActionResult Impersonate(User user)
        {
            FormsAuthentication.SignOut();
            var cookie = FormsAuthentication.GetAuthCookie(user.UserName, false);
            Response.AppendCookie(cookie);
            Session["user"] = userRepo.Get(new Specification<User>(x => x.UserName == user.UserName));

            TempData["impersonateMessage"] = new Message()
            {
                Type = "success",
                Content = string.Format("You are now impersonating {0}({1})", GetUser().DisplayName, GetUser().CustomerNumber)
            };

            return RedirectToAction("Index", "Home");
        }

        public ActionResult ImpersonateTile()
        {
            if (Roles.IsUserInRole("RepGroup"))
            {
                var rep = ((RepGroupUser)GetUser()).RepGroupNumber;
                var custs = _customerRepo.GetAll(rep);
                ViewData["custs"] = custs.ToList();
            }
            return PartialView();
        }

        [HttpPost]
        public ActionResult ImpersonateTile(ImpersonateUserModel model)
        {
            var customer = _customerRepo.Get(model.CustomerNumber.PadSAP(10).ToUpper());
            if (customer != null)
            {
                GetUser().CustomerNumber = model.CustomerNumber.ToUpper();

                TempData["impersonateMessage"] = new Message()
                {
                    Type = "success",
                    Content = string.Format("You are now impersonating {0}({1})", customer.Name, model.CustomerNumber.ToUpper())
                };
            }
            else
            {
                TempData["impersonateMessage"] = new Message()
                {
                    Type = "failure",
                    Content = string.Format("Customer {0} does not exist.", model.CustomerNumber.ToUpper())
                };
            }

            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ForgotPassword(string UserName, string returnUrl)
        {
            if (ModelState.IsValid && WebSecurity.UserExists(UserName))
            {
                //_userActivityRepo.Add(new UserActivity()
                //{
                //    User = model.UserName,
                //    Action = "Login",
                //    Message = model.UserName + " logged in"
                //});

                var key = Guid.NewGuid();
                _passwordResetRepo.Add(new PasswordReset()
                {
                    PrivateKey = key,
                    UserName = UserName
                });

                EmailService.SendMessage("Reset Your Password", "Use the link below to reset your password:\n\n http://customer.westchestergear.com/Account/ResetPassword?key=" + key, UserName);

                return RedirectToLocal(returnUrl);
            }

            // If we got this far, something failed, redisplay form
            ModelState.AddModelError("", "There is no user matching the provided user name.");
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult ResetPassword(string key)
        {
            var reset = _passwordResetRepo.Get(Guid.Parse(key));

            var resetModel = new ResetPasswordModel()
            {
                UserName = reset.UserName,
                Key = reset.PrivateKey
            };
            return View(resetModel);
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult ResetPassword(ResetPasswordModel model)
        {
            var reset = _passwordResetRepo.Get(model.Key);
            if (ModelState.IsValid)
            {
                // reset the users password
                string token = WebSecurity.GeneratePasswordResetToken(model.UserName);
                WebSecurity.ResetPassword(token, model.NewPassword);

                // remove their private key
                _passwordResetRepo.Remove(reset);

                // Log the user in
                var cookie = FormsAuthentication.GetAuthCookie(model.UserName, false);
                Response.AppendCookie(cookie);
                Session["user"] = userRepo.Get(new Specification<User>(x => x.UserName == model.UserName));

                TempData["resetPasswordMessage"] = new Message()
                {
                    Type = "success",
                    Content = "Your password has been reset successfully. You are now logged in."
                };
                return RedirectToAction("Index", "Home");
            }
            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult ActivateAccount(string key)
        {
            var reset = _passwordResetRepo.Get(Guid.Parse(key));

            var resetModel = new ResetPasswordModel()
            {
                UserName = reset.UserName,
                Key = reset.PrivateKey
            };
            return View(resetModel);
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult ActivateAccount(ResetPasswordModel model)
        {
            var reset = _passwordResetRepo.Get(model.Key);
            if (ModelState.IsValid)
            {
                // reset the users password
                string token = WebSecurity.GeneratePasswordResetToken(model.UserName);
                WebSecurity.ResetPassword(token, model.NewPassword);

                // remove their private key
                _passwordResetRepo.Remove(reset);

                // Log the user in
                var cookie = FormsAuthentication.GetAuthCookie(model.UserName, false);
                Response.AppendCookie(cookie);
                Session["user"] = userRepo.Get(new Specification<User>(x => x.UserName == model.UserName));

                TempData["resetPasswordMessage"] = new Message()
                {
                    Type = "success",
                    Content = "Your account has been activated. You are now logged in."
                };
                return RedirectToAction("Index", "Home");
            }
            return View(model);
        }

        public ActionResult DeleteAccount(string userName)
        {
            try
            {
                // TODO: Add delete logic here
                if (Roles.GetRolesForUser(userName).Count() > 0)
                {
                    Roles.RemoveUserFromRoles(userName, Roles.GetRolesForUser(userName));
                }

                var user = userRepo.Get(new Specification<User>(x => x.UserName == userName));
                userRepo.Delete(user);
                ((SimpleMembershipProvider)Membership.Provider).DeleteAccount(userName); // deletes record from webpages_Membership table
                ((SimpleMembershipProvider)Membership.Provider).DeleteUser(userName, true); // deletes record from UserProfile table

                return RedirectToAction("Index", "User");
            }
            catch
            {
                return View(userName);
            }
        }

        public ActionResult DenyRequest(string userName)
        {
            var request = _accountRequestRepo.Get(userName);

            request.Status = "Denied";
            _accountRequestRepo.Save(request);

            EmailService.SendMessage("Your West Chester Protective Gear Account Request", "Sorry, but your account request has been denied.\n\nIf you feel that this is a mistake please contact your account manager. We apologize for any inconvenience.", request.Email);

            return View("AccountRequests");
        }

        #region Helpers
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
        }

        internal class ExternalLoginResult : ActionResult
        {
            public ExternalLoginResult(string provider, string returnUrl)
            {
                Provider = provider;
                ReturnUrl = returnUrl;
            }

            public string Provider { get; private set; }
            public string ReturnUrl { get; private set; }

            public override void ExecuteResult(ControllerContext context)
            {
                OAuthWebSecurity.RequestAuthentication(Provider, ReturnUrl);
            }
        }

        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
        #endregion
    }
}
