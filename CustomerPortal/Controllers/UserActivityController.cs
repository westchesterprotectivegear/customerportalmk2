﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CustomerPortal.Controllers
{
    public class UserActivityController : _ApplicationController
    {
        private readonly IUserActivityRepository _userActivityRepo;

        public UserActivityController(IUserActivityRepository userActivityRepo)
        {
            _userActivityRepo = userActivityRepo;
        }

        public ActionResult Index()
        {
            return View();
        }

    }
}
