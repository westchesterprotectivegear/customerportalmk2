﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CustomerPortal.Models;
using System.IO;
using OfficeOpenXml;
using System.Text.RegularExpressions;
using CustomerPortalModels;
using SAP.Middleware.Connector;
using System.Web.Security;
using NCommon.Specifications;
using System.Data.SqlClient;
using System.Configuration;

namespace CustomerPortal.Controllers
{
    [Authorize]
    public class ShoppingCartController : _ApplicationController
    {
        private readonly IShoppingCartRepository _cartRepo;
        private readonly IProductRepository _productRepo;
        private readonly IOrderRepository _orderRepo;
        private readonly IOrderLineRepository _orderLineRepo;
        private readonly ICustomerRepository _customerRepo;
        private readonly IUserActivityRepository _userActivityRepo;
        private readonly ICheckoutDataRepository _checkoutDataRepo;

        public ShoppingCartController(IShoppingCartRepository cartRepo, IProductRepository productRepo, IOrderRepository orderRepo, IOrderLineRepository orderLineRepo, ICustomerRepository customerRepo, IUserActivityRepository userActivityRepo, ICheckoutDataRepository checkoutDataRepo)
        {
            _cartRepo = cartRepo;
            _productRepo = productRepo;
            _orderRepo = orderRepo;
            _orderLineRepo = orderLineRepo;
            _customerRepo = customerRepo;
            _userActivityRepo = userActivityRepo;
            _checkoutDataRepo = checkoutDataRepo;
        }

        public ActionResult Index()
        {
            return View(GetCart());
        }

        [HttpPost]
        public ActionResult CheckoutList()
        {
            var checkouts = _checkoutDataRepo.GetAll().Where(x => !x.Success);
            return View(checkouts);
        }

        private ShoppingCart GetCart()
        {
            if (!string.IsNullOrEmpty(GetUser().CustomerNumber))
            {
                var cart = _cartRepo.Get(GetUser().UserName);

                if (Session["shoppingCart"] != null && ((ShoppingCart)Session["shoppingCart"]).Equals(cart))
                    return (ShoppingCart)Session["shoppingCart"];

                for (int i = 0; i < cart.LineItems.Count; i++)
                {
                    var line = cart.LineItems[i];
                    line.Product = _productRepo.Get(line.SKU, GetUser().CustomerNumber);

                    // if the product is no longer valid, remove the line item.
                    if (line.Product == null)
                    {
                        _cartRepo.RemoveItemFromCart(line.SKU, GetUser().UserName);

                        // Get the cart again to reflect removed line items
                        cart = _cartRepo.Get(GetUser().UserName);
                    }
                }

                Session["shoppingCart"] = cart;
                return cart;
            }
            else
                return new ShoppingCart();
        }

        public ActionResult ShoppingCartIcon()
        {
            ShoppingCart cart = GetCart();

            return PartialView(cart);
        }

        [HttpPost]
        public ActionResult AddToCart(AddToCartModel model)
        {
            Message message;

            model.SKU = model.SKU.ToUpper();
            var product = _productRepo.Get(model.SKU, GetUser().CustomerNumber);

            if (product != null)
            {
                // if quantity less than 1, make 1. Capitalize sku
                model.Quantity = model.Quantity < 1 ? 1 : model.Quantity;
                model.SKU = model.SKU;

                // store in persistent cart
                _cartRepo.AddItemToCart(model.SKU, model.Quantity, GetUser().UserName, false);

                message = new Message()
                {
                    Type = "Success",
                    Title = "Item Added",
                    Content = string.Format("{0} of {1} added to your cart.", model.Quantity, model.SKU)
                };
            }
            else
            {
                message = new Message()
                {
                    Type = "Failure",
                    Title = "Item Not Added to Cart",
                    Content = string.Format("Item '{0}' does not exist.", model.SKU)
                };
            }

            return Json(new { message = message, view = RenderViewToString("ShoppingCartIcon", GetCart()) });
        }

        [HttpPost]
        public ActionResult AddSampleToCart(string sku)
        {
            var product = _productRepo.Get(sku, GetUser().CustomerNumber);

            // store in persistent cart
            _cartRepo.AddItemToCart(sku, 1, GetUser().UserName, true);

            var message = new Message()
            {
                Type = "success",
                Title = "Item Added",
                Content = string.Format("A sample of {0} added to your cart.", sku)
            };

            return Json(new { message = message, view = RenderViewToString("ShoppingCartIcon", GetCart()) });
        }

        [HttpPost]
        public ActionResult UpdateLineItem(AddToCartModel model)
        {
            _cartRepo.UpdateLineItem(model.SKU, model.Quantity, GetUser().UserName);
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult RemoveLineItem(CustomerPortalModels.Product product)
        {
            _cartRepo.RemoveItemFromCart(product.SKU, GetUser().UserName);
            return RedirectToAction("Index");
        }

        public ActionResult ClearCart()
        {
            _cartRepo.ClearCart(GetUser().UserName);
            return View("Index", GetCart());
        }

        public ActionResult Checkout()
        {
            return View();
        }

        [HttpGet]
        public ActionResult CheckoutStep1()
        {
            CheckoutStep1 model = new CheckoutStep1();
            model.Email = User.Identity.Name;
            model.CompanyName = _customerRepo.Get(GetUser().CustomerNumber).Name;
            return PartialView(model);
        }

        [HttpPost]
        public ActionResult CheckoutStep1(CheckoutStep1 model)
        {
           /* string q = "SELECT * FROM prd.VAKPA WHERE(MANDT = '100') AND(KUNDE = @custnum) AND(PARVW = 'AG') AND(BSTNK = @ponum)";
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SAPConnection"].ConnectionString);
            conn.Open();
            SqlCommand cmd = new SqlCommand(q, conn);
            cmd.Parameters.Add(new SqlParameter("@ponum", System.Data.SqlDbType.VarChar));
            cmd.Parameters["@ponum"].Value = model.PONumber;
            cmd.Parameters.Add(new SqlParameter("@custnum", System.Data.SqlDbType.VarChar));
            cmd.Parameters["@custnum"].Value = GetUser().CustomerNumber;
            SqlDataReader reader = cmd.ExecuteReader();

            if (reader.HasRows)
            {
                ModelState.AddModelError("", "PO Number Must be unique.");
                return PartialView("CheckoutStep1", model);
            }
            */
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "The highlighted fields are required..");
                return PartialView("CheckoutStep1", model);
            }
            else
            {
                var checkout = GetCheckout();
                checkout.Attention = model.Attention;
                checkout.CompanyName = model.CompanyName;
                checkout.PONumber = model.PONumber;
                checkout.CustomerPONumber = model.CustomerPONumber;
                checkout.Email = model.Email;
                checkout.PhoneNumber = model.PhoneNumber;
                checkout.PaymentTerms = model.PaymentTerms;
                checkout.AccountNumber = model.AccountNumber;
                checkout.Carrier = model.Carrier;
                checkout.ShippingInstructions = model.ShippingInstructions;
                return Json(new { success = true });
            }
        }

        [HttpPost]
        public ActionResult CheckoutStep2(CheckoutStep2 model)
        {
            if (!ModelState.IsValid)
            {
                return PartialView("CheckoutStep2", model);
            }
            else
            {
                var checkout = GetCheckout();
                checkout.ShipToID = model.ShipToID;
                checkout.Street = model.Street;
                checkout.City = model.City;
                checkout.State = model.State;
                checkout.Zip = model.Zip;
                checkout.Name = model.Name;
                return Json(new { success = true, view = RenderViewToString("ConfirmCheckout", GetCheckout()) });
            }
        }

        [HttpPost]
        public ActionResult SelectShippingAddress(CheckoutModel model, string BtnPrevious, string BtnNext)
        {
            var checkout = GetCheckout();
            if (BtnPrevious != null)
            {
                return View("Checkout", checkout);
            }
            if (BtnNext != null)
            {
                if (ModelState.IsValid)
                {
                    checkout = model;
                    return View("SelectShippingAddress", model);
                }
            }

            return View();
        }

        [HttpPost]
        public ActionResult ConfirmCheckout(CheckoutModel model)
        {
            CheckoutResponse response = new CheckoutResponse();
            var notice_email = model.NotificationEmail;
            try
            {
                model = GetCheckout();

                model.Cart = GetCart();

                var checkout = new CheckoutData()
                {
                    Data = SerializationService.DataContractSerializeObject<CheckoutModel>(model),
                    UserName = GetUser().UserName,
                    NotificationEmail = notice_email,
                    Browser = Request.Browser.Browser + " " + Request.Browser.MajorVersion

                };
                _checkoutDataRepo.Add(checkout);

                model.Telesales = false;
                if (User.IsInRole("Telesales"))
                    model.Telesales = true;

                //response = new OrderService().PlaceOrder(model, GetCart());


                var salesman = _customerRepo.Get(GetUser().CustomerNumber).Salesman;
                //var salesman = "nmoore@westchestergear.com";

                if (salesman == null)
                    salesman = "";

                string body = "";
                body += User.Identity.Name + ", has placed an order on behalf of " + model.CompanyName + "\n \n";
                body += model.Attention + "\n";
                body += model.Street + "\n" + model.City + ", " + model.State + " " + model.Zip + "\n \n";
                body += "Attention:  " + model.Attention + "\n";
                body += "PONumber:  " + model.PONumber + "\n";
                body += "CustomerPONumber:  " + model.CustomerPONumber + "\n";
                body += "Email:  " + model.Email + "\n";
                body += "PhoneNumber:  " + model.PhoneNumber + "\n";
                body += "PaymentTerms:  " + model.PaymentTerms + "\n";
                body += "AccountNumber:  " + model.AccountNumber + "\n";
                body += "ShippingInstructions:  " + model.ShippingInstructions + "\n \n \n";
                body += "Items In Order: \n";
                body += "SKU                 QTY   SAMPLE       TOTAL \n";
                body += "------------------------------------------------------------------------------------------------ \n";
                foreach (var item in model.Cart.LineItems)
                {
                    body += item.SKU + new string(' ', 20 - item.SKU.Length) + item.Quantity+" "+item.Product.UnitOfMeasure + new string(' ', 6 - (item.Quantity.ToString() + " " + item.Product.UnitOfMeasure).Length) + item.Sample.ToString() + new string(' ', 13 - item.Sample.ToString().Length) + item.TotalPrice.ToString() + "\n";
                }

                body += "Please input this order into Epicor Eclipse. \n";

                EmailService.SendMessage("Customer " + GetUser().CustomerNumber + " entered a new order via the Customer Portal", body, new List<string> { "orders@pipusa.com", salesman});

                response.OrderResponse = new OrderSubmissionResponse();
                response.OrderResponse.Status = true;

                

                if (response.OrderResponse != null && response.OrderResponse.Status)
                {
                    try
                    {
                        checkout.OrderNumber = "TBD";
                        //checkout.OrderNumber = response.OrderResponse.OrderNumber;

                        //string pdf = new OrderService().PrintOrder(response.OrderResponse.OrderNumber);

                        EmailService.SendMessage("Customer " + GetUser().CustomerNumber + " entered a new order via the Customer Portal",
                            string.Format("Order Number: {0}\nPlaced By: {1}\nShipping Instructions: {2}", response.OrderResponse.OrderNumber, GetUser().UserName, model.ShippingInstructions),
                            new List<string> { "mmoore@westchestergear.com" });

                        /*EmailService.SendMessage("Order Confirmation",
                            "Your order has been placed. \n \n  You will recieve a confirmation once processed into our systems. \n \n Thank you for your patience ",
                            new List<string> { GetUser().UserName });
                            */
                        _userActivityRepo.Add(new UserActivity()
                        {
                            User = User.Identity.Name,
                            Action = "Order Submission",
                            Message = "Placed order "
                        });
                        notice_email = "";
                        if (notice_email != null && notice_email != "")
                        {
                            string q = "Insert Into prd.ZNOTIFY (MANDT,VBELN,ERDAT,ERZET,EMAIL) Values(@client,@ordernum,@date,@time,@notifyemail)";
                            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SAPConnection"].ConnectionString);
                            conn.Open();
                            SqlCommand cmd = new SqlCommand(q, conn);
                            cmd.Parameters.Add(new SqlParameter("@client", System.Data.SqlDbType.VarChar));
                            cmd.Parameters["@client"].Value = "100";
                            cmd.Parameters.Add(new SqlParameter("@ordernum", System.Data.SqlDbType.VarChar));
                            cmd.Parameters["@ordernum"].Value = response.OrderResponse.OrderNumber.PadLeft(10,'0');
                            cmd.Parameters.Add(new SqlParameter("@notifyemail", System.Data.SqlDbType.VarChar));
                            cmd.Parameters["@notifyemail"].Value = notice_email;
                            cmd.Parameters.Add(new SqlParameter("@date", System.Data.SqlDbType.VarChar));
                            cmd.Parameters["@date"].Value = DateTime.Now.ToString("yyyyMMdd");
                            cmd.Parameters.Add(new SqlParameter("@time", System.Data.SqlDbType.VarChar));
                            cmd.Parameters["@time"].Value = DateTime.Now.ToString("HHmmss");
                            cmd.ExecuteNonQuery();
                            conn.Close();
                        }
                    }
                    catch (Exception e)
                    {
                        EmailService.SendMessage("Error completing order " + response.OrderResponse.OrderNumber, e.Message + "\n" + e.StackTrace);
                    }


                    // if the order can't be pulled from SAP return a null
                    try
                    {
                        response.Order = _orderRepo.Get(response.OrderResponse.OrderNumber, GetUser().CustomerNumber);
                        response.Order.LineItems = _orderLineRepo.Get(response.OrderResponse.OrderNumber).ToList();
                    }
                    catch
                    {

                        response.Order = null;
                    }
                }

                if (response.SampleResponse != null && response.SampleResponse.Status)
                {

                    EmailService.SendMessage("Customer " + GetUser().CustomerNumber + " entered a sample request via the Customer Portal",
                        string.Format("Request Number: {0}\nPlaced By: {1}", response.SampleResponse.OrderNumber, GetUser().UserName),
                        new List<string> { salesman, "mmoore@westchestergear.com" });

                    EmailService.SendMessage("Sample Request Confirmation",
                        "Your sample request has been submitted.", new List<string> { GetUser().UserName });

                    _userActivityRepo.Add(new UserActivity()
                    {
                        User = User.Identity.Name,
                        Action = "Sample Request",
                        Message = "Request samples: "
                    });

                    checkout.OrderNumber = "TBD";
                }

                if (true)
                {
                    ClearCart();
                    checkout.Success = true;
                    _checkoutDataRepo.Save(checkout);
                }

                return View("OrderConfirmation", null, response);
            }
            catch (Exception e)
            {
                EmailService.SendMessage("Error Processing Order", e.Message + "\n" + e.InnerException + "\n" + e.StackTrace);
                ClearCart();
                return View("OrderConfirmation", response);
            }
        }

        public ActionResult ProcessCheckout(string ID)
        {
            var checkout = _checkoutDataRepo.Get(new Guid(ID));

            try
            {
                var model = (CheckoutModel)SerializationService.DataContractDeserializeObject<CheckoutModel>(new CheckoutModel(), checkout.Data);

                var originUser = userRepo.Get(new Specification<User>(x => x.UserName == checkout.UserName));

                model.Telesales = false;
                if (Roles.IsUserInRole(originUser.UserName, "Telesales"))
                    model.Telesales = true;

                CheckoutResponse response = new OrderService().PlaceOrder(model, model.Cart);

                var salesman = _customerRepo.Get(originUser.CustomerNumber).Salesman;
                //var salesman = "nmoore@westchestergear.com";

                if (salesman == null)
                    salesman = "";

                if (response.OrderResponse != null && response.OrderResponse.Status)
                {
                    response.Order = _orderRepo.Get(response.OrderResponse.OrderNumber, originUser.CustomerNumber);
                    response.Order.LineItems = _orderLineRepo.Get(response.OrderResponse.OrderNumber).ToList();

                    _userActivityRepo.Add(new UserActivity()
                    {
                        User = User.Identity.Name,
                        Action = "Order Submission",
                        Message = "Placed order: " + response.OrderResponse.OrderNumber
                    });

                    string pdf = new OrderService().PrintOrder(response.OrderResponse.OrderNumber);

                    //EmailService.SendMessage("Customer " + originUser.CustomerNumber + " entered a new order via the Customer Portal",
                    //    string.Format("Order Number: {0}\nPlaced By: {1}\nShipping Instructions: {2}", response.OrderResponse.OrderNumber, originUser.UserName, model.ShippingInstructions),
                    //    new List<string> { salesman, "nmoore@westchestergear.com" }, new List<string> { @"\\fred\reprint\" + pdf });

                    //EmailService.SendMessage("Order Confirmation",
                    //    "Your order has been placed",
                    //    new List<string> { originUser.UserName }, new List<string> { @"\\fred\reprint\" + pdf });

                    checkout.OrderNumber = response.OrderResponse.OrderNumber;
                }

                if (response.SampleResponse != null && response.SampleResponse.Status)
                {
                    _userActivityRepo.Add(new UserActivity()
                    {
                        User = User.Identity.Name,
                        Action = "Sample Request",
                        Message = "Request samples: " + response.SampleResponse.OrderNumber
                    });

                    EmailService.SendMessage("Customer " + originUser.CustomerNumber + " entered a sample request via the Customer Portal",
                        string.Format("Request Number: {0}\nPlaced By: {1}", response.SampleResponse.OrderNumber, originUser.UserName),
                        new List<string> { salesman, "mmoore@westchestergear.com" });

                    EmailService.SendMessage("Sample Request Confirmation",
                        "Your sample request has been submitted.", new List<string> { originUser.UserName });

                    checkout.OrderNumber = response.SampleResponse.OrderNumber;
                }

                if (response.Status)
                {
                    ClearCart();
                    checkout.Success = true;
                    _checkoutDataRepo.Save(checkout);
                }

                return View("OrderConfirmation", null, response);
            }
            catch (Exception e)
            {
                EmailService.SendMessage("Error Processing Order", e.Message + "\n" + e.InnerException + "\n" + e.StackTrace);
                return View();
            }
        }

        private CheckoutModel GetCheckout()
        {
            if (Session["checkout"] == null)
            {
                Session["checkout"] = new CheckoutModel();
            }

            return (CheckoutModel)Session["checkout"];
        }

        [HttpPost]
        public ActionResult CheckForExistingPO(string po)
        {
            return Json(new { success = _orderRepo.OrderExists(GetUser().CustomerNumber, po) });
        }

        public ActionResult ImportShoppingList()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ImportShoppingList(HttpPostedFileBase file)
        {
            if (file.ContentLength > 0)
            {
                BinaryReader b = new BinaryReader(file.InputStream);
                var package = new ExcelPackage(file.InputStream);
                var book = package.Workbook;
                var sheet = book.Worksheets[1];

                int row = 2;
                while (sheet.Cells[row, 1].Value != null && !string.IsNullOrEmpty(sheet.Cells[row, 1].Value.ToString()))
                {
                    string sku = sheet.Cells[row, 1].Value.ToString().ToUpper();

                    if (sheet.Cells[row, 2].Value != null && Regex.IsMatch(sheet.Cells[row, 2].Value.ToString(), "^(0|[1-9][0-9]*)$") && Convert.ToInt32(sheet.Cells[row, 2].Value) > 0)
                    {
                        AddToCart(new AddToCartModel()
                        {
                            SKU = sku,
                            Quantity = Convert.ToInt32(sheet.Cells[row, 2].Value)
                        });
                    }

                    row++;
                }
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public int GetAvailability(string sku)
        {
            sku = sku.ToUpper();

            try
            {
                RfcDestination dest = RfcDestinationManager.GetDestination("mySAPdestination");
                RfcRepository repo = dest.Repository;
                IRfcFunction testfn = repo.CreateFunction("MD_STOCK_REQUIREMENTS_LIST_API");

                testfn.SetValue("MATNR", sku.ToUpper().PadSAP(18));
                testfn.SetValue("WERKS", "MONR");

                testfn.Invoke(dest);
                var table = testfn.GetTable("MDEZX").ToDataTable("mrpdetail");

                int runningTotal = 0;
                DateTime today = DateTime.Now;
                DateTime nextWeek = today.AddDays(7);

                for (int i = 0; i < table.Rows.Count; i++)
                {
                    var row = table.Rows[i];

                    if (row["DELB0"].ToString() == "Stock")
                    {
                        runningTotal += Convert.ToInt32(row["MNG01"]);
                    }
                }

                return runningTotal;
            }
            catch
            {
                return 1;
            }
        }
    }
}
